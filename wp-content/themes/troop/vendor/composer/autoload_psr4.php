<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'WordPlate\\Acf\\' => array($vendorDir . '/wordplate/acf/src'),
    'RD\\WP\\' => array($vendorDir . '/rohan2388/wp-css/src'),
    'MyThemeShop\\Helpers\\' => array($vendorDir . '/mythemeshop/wordpress-helpers/src'),
);

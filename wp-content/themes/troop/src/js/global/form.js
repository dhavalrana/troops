import axios from 'axios';

document.addEventListener('DOMContentLoaded', ()=>{	
	(()=>{
		const form = document.querySelector('.newsletter');
		if ( ! form ) return;
		const thanks = form.querySelector(".thanks");
		form.addEventListener('submit', (e)=> {
			e.preventDefault();
			thanks.classList.remove('is-active');


			axios.post(form.getAttribute('action'), new FormData(form)).then(response=> {
				if ( response.data.status == 'ok' ) {
					thanks.classList.add('is-active');
				}
			}).catch(e=> console.log(e.message));
		});
	})();
	
	(()=>{
		if ( ! window.settings.recaptcha_site_key ) return; // if recapctah is disabled..

		const btns = document.querySelectorAll('.recaptcha-btn');
		const insertInput = (form, token) => {
			let input = form.querySelector('input[name=token]');
			let actionInput = form.querySelector('input[name=action]');

			if ( ! input ) {
				input = document.createElement('input');
				input.setAttribute('type', 'hidden');
				input.setAttribute('name', 'token');
				input.setAttribute('value', token);
				form.appendChild(input);
			}

			if ( ! actionInput ) {
				actionInput = document.createElement('input');
				actionInput.setAttribute('type', 'hidden');
				actionInput.setAttribute('name', 'action');
				actionInput.setAttribute('value', 'form_handler');
				form.appendChild(actionInput);
			}

		}

		btns.forEach(btn=>{
			btn.addEventListener('click', (e)=>{
				e.preventDefault();
				const form = btn.closest('form');
				if ( typeof grecaptcha != 'undefined' ) {
					grecaptcha.ready(()=>{
						grecaptcha.execute(window.settings.recaptcha_site_key, {action: 'submit'}).then(function(token) {
							insertInput(form, token);
							form.dispatchEvent(new Event('submit'));
						});
					});
				}
			});
		});
	})();

});
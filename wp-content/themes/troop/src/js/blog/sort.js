document.addEventListener('DOMContentLoaded', ()=>{
	const container = document.querySelector('.blog-sort');
	if ( ! container ) return;


	const select = container.querySelector('select');
	select.addEventListener('change', (e)=>{
		const value = select.value;
		const urlParams = new URLSearchParams(window.location.search);
		urlParams.set('order', value);
		window.location.search = urlParams;
	});

});
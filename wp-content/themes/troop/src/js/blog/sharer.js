import 'sharer.js';
import copy from 'copy-to-clipboard';

document.addEventListener('DOMContentLoaded', ()=>{;
	if ( document.querySelectorAll('[data-sharer]').length ) {
		window.Sharer.init();
	}
	
	document.querySelectorAll('[data-copy]').forEach(item => {
		item.addEventListener('click', (e)=> {
			e.preventDefault();
			const url = item.dataset.url;
			let active = false;

			if ( copy(url) && ! active ) {
				console.log(item);
				active = true;
				item.classList.add('tooltip-active');
				setTimeout(()=> {
					item.classList.remove('tooltip-active');
					active = false;
				}, 2000);
			}

		});
	});


});
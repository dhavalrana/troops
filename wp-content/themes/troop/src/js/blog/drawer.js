document.addEventListener('DOMContentLoaded', ()=>{
	const container = document.querySelector('.page-container.has-drawer');
	if ( ! container ) return;


	const openBtns = container.querySelectorAll('[data-drawer-open]');
	openBtns.forEach(btn=>{
		btn.addEventListener('click', (e)=>{
			e.preventDefault();
			container.classList.add('drawer-open');
		});
	});

	const closeBtns = container.querySelectorAll('[data-drawer-close]');
	closeBtns.forEach(btn=>{
		btn.addEventListener('click', (e)=>{
			e.preventDefault();
			container.classList.remove('drawer-open');
		});
	});
});
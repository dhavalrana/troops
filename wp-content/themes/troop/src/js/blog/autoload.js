import axios from 'axios';
import {refresh} from './masonry';

document.addEventListener('DOMContentLoaded', ()=>{
	const container = document.querySelector('.masonry');
	if ( ! container ) return;
	let next = container.dataset.next;
	let isLoading = false;

	const parseHtml = (str) => {
		const context = document.implementation.createHTMLDocument();
		const base = context.createElement('base');
		base.href = document.location.href;
		context.head.appendChild(base);	  
		context.body.innerHTML = str;
		return context.body;
	}

	const loadNext = () => {
		if ( isLoading || ! next ) return;
		isLoading = true;

		axios.get(next).then(response=> {
			const html = parseHtml( response.data );
			const newContainer = html.querySelector('.masonry');
			next = newContainer.dataset.next;
			newContainer.querySelectorAll('.masonry-item').forEach(item=> {
				container.append(item);
			});
			refresh();
			isLoading = false;
		}).catch(e=>console.log(e.messahe));
	}

	window.addEventListener('scroll', ()=>{
		let rect = container.getBoundingClientRect();
		let adjust = 100;
		if ( ( window.innerHeight + window.scrollY + adjust ) >= rect.bottom ) {
			loadNext();
		}
	});
});
import Macy from 'macy';
import imagesloaded from 'imagesloaded';

let instance = null;
let container = null;
document.addEventListener('DOMContentLoaded', ()=>{
	const section = document.querySelector('.blog-masonry');
	if ( ! section ) return;
	container = section.querySelector('.masonry');

	instance = new Macy({
		container: container,
		columns: 3,
		trueOrder: true,
		margin: 30,
		breakAt: {
			960: {
				columns: 2,
			},

			767: {
				columns: 1,
			}

		}
	});
	imagesloaded(container, ()=>{
		instance.recalculate(true);
	});
	
});
export const refresh = () => {
	if ( instance ) {
		instance.recalculate(true);	
		imagesloaded(container, ()=>{
			instance.recalculate(true);
		});

	}
}
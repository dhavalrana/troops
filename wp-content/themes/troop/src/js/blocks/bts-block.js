import axios from "axios";

document.addEventListener("DOMContentLoaded", () => {
  const container = document.querySelector(".bts-block");
  if (!container) return;

  const triggers = container.querySelectorAll(".card-trigger");
  const cards = container.querySelectorAll(".card-content");

  triggers.forEach((trigger) => {
    trigger.addEventListener("click", (e) => {
      e.preventDefault();
      const trigger = e.currentTarget;
      const target = trigger.dataset.card;

      triggers.forEach((item) => item.classList.remove("is-active"));
      trigger.classList.add("is-active");

      cards.forEach((card) => {
        if (card.dataset.card == target) {
          card.classList.add("is-active");
        } else {
          card.classList.remove("is-active");
        }
      });
    });
  });

  triggers[0].click();
  container.querySelectorAll(".demo-trigger").forEach((item) => {
    item.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(".book-a-demo-button").click();
    });
  });

  const swiper = container.querySelector(".bts-card-mobile .swiper");
  const swiperInstance = new Swiper(swiper, {
    speed: 400,
    spaceBetween: 100,
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
      el: container.querySelector(".swiper-pagination"),
    },
  });

  const forms = container.querySelectorAll("form");
  forms.forEach((form) => {
    const thanks = form.querySelector(".field-thanks");
    form.addEventListener("submit", (e) => {
      e.preventDefault();
      thanks.classList.remove("is-active");
      axios
        .post(form.getAttribute("action"), new FormData(form))
        .then((response) => {
          if (response.data.status == "ok") {
            window.location.href = "/blog";
            thanks.classList.add("is-active");
          }
        })
        .catch((e) => console.log(e.message));
    });
  });
});

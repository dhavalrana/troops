<?php 

/**
 * Loads js and css 
 * 
 * @version 1.0.0
 * @author Rohan Das <rohan@kraftdesignsmiths.com>
 */
class Enqueue {
	use Traits\Singleton;

	protected $js_list = [];
	protected $css_list = [];
	protected $js_vars = [];

	/**
	 * Add timestamp as version when in dev mode
	 *
	 * @param boolean|null $force
	 * @return string
	 * @since 1.0.0
	 */
	public function version( ?bool $force = null ) : string {
		return ( ( ! isset( $force ) && Helper::is_dev() ) || ( $force === true ) ) ? time() : wp_get_theme()->get( 'Version' );;
	}

	/**
	 * Init
	 * 
	 * @since 1.0.0
	 */
	public function __construct(){
		add_action('wp_enqueue_scripts', [$this, 'load_js']);
		add_action('wp_enqueue_scripts', [$this, 'load_css']);
		// add_action('admin_enqueue_scripts', [$this, 'load_admin_css']);
	}

	/**
	 * Load js
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function load_js(): void {		
		wp_enqueue_script( 'swiper', 'https://unpkg.com/swiper/swiper-bundle.min.js', [], $this->version(false) );
		wp_enqueue_script( 'chunk1',  RD_JS_URI . '/1.1ad9e7d1.chunk.js', [], $this->version(false) );
		wp_enqueue_script( 'chunk2',  RD_JS_URI . '/app.59b28564.js', [], $this->version(false) );
		wp_enqueue_script( 'main',  RD_JS_URI . '/main.js', [], $this->version() );
		wp_localize_script('main', 'settings', array_merge([
			'ajax' => admin_url( 'admin-ajax.php' ),
		], $this->js_vars));

		if ( ! empty( $this->js_list ) ) {
			foreach( $this->js_list as $k => $v ) {
				$internal = ( ! strpos($k, 'http') === 0);
				$v = $internal ?  RD_JS_URI . $v : $v;
				wp_enqueue_script( "troop-{$k}",  $v, [], $this->version($internal) );
			}
		}
	}

	/**
	 * Load css
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function load_css(): void {
		wp_enqueue_style( 'swiper',  'https://unpkg.com/swiper/swiper-bundle.min.css', [], $this->version(false) );
		wp_enqueue_style( 'main',  RD_CSS_URI . '/main.css', [], $this->version() );
		wp_enqueue_style( 'main-v2',  RD_CSS_URI . '/main-v2.css', [], $this->version() );
	}

	/**
	 * Load css
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function load_admin_css(): void {
		wp_enqueue_style( 'admin',  RD_CSS_URI . '/admin.css', [], $this->version(false) );
	}

	/**
	 * Enqueue js
	 */
	public static function js(string $key, string $url) {
		$instance = self::instance();
		$instance->js_list[$key] = $url;
	}

	public static function js_vars( string $handle, $data ) {
		$instance = self::instance();
		$instance->js_vars[$handle] = $data;
	}
}
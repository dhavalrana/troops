<?php 
use WordPlate\Acf\Location;
use \RD\WP\Styles;
use WordPlate\Acf\Fields\Tab as _Tab;


if ( ! defined('RD_BLOCKS') ) {
	define('RD_BLOCKS', RD_ROOT . '/blocks' );
}

/**
 * Create blocks using ACF Pro plugin
 * 
 * @version 1.0.0
 * @author Rohan Das <rohan@kraftdesignsmiths.com>
 */
class Blocks {
	use Traits\Singleton;

	/**
	 * Blocks config collection 
	 *
	 * @var array
	 */
	protected $blocks = [];

	/**
	 * Init
	 * @since 1.0.0
	 */
	public function __construct(){
		add_action( 'init', [$this, 'load_blocks'], 15 );
		add_action( 'init', [$this, 'register_blocks'], 15 );
	}
	
	/**
	 * Load all the blocks
	 *
	 * @return void
	 */
	public function load_blocks(): void {
		foreach( glob(RD_BLOCKS . '/*/block.php') as $file ) {
			require_once $file;
		}
	}
	
	/**
	 * Register blocks with ACF
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function register_blocks(){
		foreach( $this->blocks as $block ) {
			acf_register_block_type($block);
		}
	}
	

	/**
	 * Render the block
	 *
	 * @param array $options
	 * @return void
	 * @since 1.0.0
	 */
	public function render_callback( array $options ): void {
		global $RD_BLOCK, $RD_BLOCK_ID;		

		$name = str_replace('acf/', '', $options['name']);
		$template =  RD_BLOCKS . "/{$name}/template.php";	
		$data = get_fields();
		if ( ! $data ) {
			$data = [];
		}

		$RD_BLOCK = $name;
		$RD_BLOCK_ID = 'block-'.uniqid();

		if ( is_admin() ) {	
			$title = field('block-title');		
			if ( $title ) {
				$title = " :: {$title}";
			}
			echo sprintf( '
				<div  class="block-preview">
					<div class="block-preview_title">%s %s</div>
				</div>
			', $options['title'], $title );

		} else {		
			Styles::init();
			$id = field('block-id');
			$margin = field('block-margin');
			if ( $margin ) {
				$margin = Helper::parse_value_value( $margin );
				foreach( $margin as $m ) {
					Styles::add_rule()->add_prop('margin', $m['value'], $m['query'] );
				}
			}
			if ( ! $id ) {
				$id = $name;
			}

			echo sprintf('<div class="troop-block %s" id="%s">', Styles::class(), $id);
			echo $this->render( $template, $data );
			echo '</div>';
			Styles::done();
		}
		
	}

	/**
	 * Load template file
	 *
	 * @param string $file
	 * @param array $fields
	 * @return string
	 */
	public function render( string $file, $fields = array() ): string {	
		$file = str_replace( RD_ROOT, '', $file );
		$located = locate_template( $file );
		if ( file_exists( $located ) ) {
			ob_start();		
			include($located);
			return ob_get_clean();
		}
		return '';
	}

	/**
	 * Block custom category
	 */
	const CATEGORY_SLUG = 'troop';
	const CATEGORY = 'Troop';
	/**
	 * Register custom category
	 *
	 * @param array $categories
	 * @param WP_Post $post
	 * @return array
	 * @since 1.0.0
	 */
	public function custom_block_category( $categories, $post ){
		if ( $post->post_type !== 'page' ) {
			return $categories;
		}

		return array_merge(
			$categories,
			array(
				array(
					'slug' => self::CATEGORY_SLUG,
					'title' => self::CATEGORY,
					'icon'  => 'wordpress',
				),
			)
		);
	}


	
	//////////////////////////////
	// Static functions
	//////////////////////////////

	/**
	 * Register a block
	 *
	 * @param string $title
	 * @param array $fields
	 * @param array $args
	 * @return void
	 * @since 1.0.0
	 */
	public static function register( string $title, array $fields = [], array $args = [] ): void {
		$instance = self::instance();
		$key = array_search(__FUNCTION__, array_column(debug_backtrace(), 'function'));
		$file = debug_backtrace()[$key]['file'];
		$name = basename( dirname( $file ) );

		$options = [
			'name' => $name,
			'title' => "{$title}",
			'category' => self::CATEGORY_SLUG,
			'keywords' => [self::CATEGORY],
			'render_callback' => [ $instance, 'render_callback' ],
			'post_types' => ['page']
		];
		$options = array_merge($options, $args);
		$instance->blocks[$name] = $options;


		// Wrap all block fields inside general tab by default if not already tabbed
		if ( ! isset( $fields[0] ) || !$fields[0] instanceof _Tab ) {
			array_unshift($fields, Acfx::field( 'Tab', 'Main'));
		}
		// Also add margin and id field to all blocks;
		array_push($fields, 
			Acfx::field( 'Tab', 'Settings'),
			Acfx::field( 'Text', 'ID', 'block-id')->instructions('Set custom block ID'),
			Acfx::field( 'Text', 'Title', 'block-title')->instructions('Set custom block ID'),
			Acfx::field( 'Text', 'Margin', 'block-margin')->instructions('Set block margin'),
			Acfx::field( 'Text', 'Padding', 'block-padding')->instructions('Set block padding')
		);

		
		$block_options = [
			'title' => "Block - {$title}",
			'fields' => $fields,
			'location' => [
				Location::if('block', "acf/{$name}")
			],
		];
		register_extended_field_group( $block_options );
	}

	/**
	 * Render block style class
	 *
	 * @return string
	 */
	public static function class(): string {
		return Styles::class();
	}

	/**
	 * Just an alias of Styles::add_rule()
	 */
	public static function css( ?string $selector = null ) {
		return Styles::add_rule( $selector );
	}

}
<?php 
/**
 * Main theme class
 * 
 * * Intiate all the modules - 
 * * *
 * * Removes admin bar when in dev mode
 * 
 * @version  1.0.0
 * @author  Rohan Das
 */
class Core {
	use Traits\Singleton;

	/**
	 * Check all the dependency
	 * * ACF Pro plugin required
	 *
	 * @return void
	 */
	public function dependency(){		
		$err = false;
		if ( ! function_exists( 'acf_add_options_page' ) ) {
			Dependency::instance()->error('ACF Pro plugin is required');
			$err = true;
		}
		return !$err;
	}

	/**
	 * Image size for small icons
	 */
	const ACF_SMALL_ICON = 'acf-small-icon';

	public function __construct(){			
		Dependency::instance();	 
		if ( $this->dependency() ) {
						
			add_theme_support( 'title-tag' );
			add_theme_support( 'post-thumbnails' );
			add_filter('show_admin_bar', [$this, 'show_admin_bar']);
			add_image_size( Core::ACF_SMALL_ICON, 50, 50, true );

			$this->load_modules();			
		}
	}

	/**
	 * Load all the modules
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function load_modules(): void {
		Settings::instance();
		Enqueue::instance();
		Blocks::instance();
		Blog::instance();
		Form::instance();
	}

	/**
	 * Hide admin bar when in development mode
	 *
	 * @return void
	 */
	public function show_admin_bar( $return ){
		if ( defined('RD_DEV') && RD_DEV === true ) {
			$return = false;
		}
		return $return;
	}



	///////////////////////////////////
	// STATIC FUNCTIONS
	///////////////////////////////////
	/**
	 * Render navbar
	 * @return void
	 * @since 1.0.0
	 */
	public static function navbar(): void {
		$str = Helper::template('global/navbar');
		echo $str;
	}
	/**
	 * Render footer
	 * @return void
	 * @since 1.0.0
	 */
	public static function footer(): void {
		$str = Helper::template('global/footer');
		echo $str;
	}

	/**
	 * Render modals
	 * @return void
	 * @since 1.0.0
	 */
	public static function modals(): void {
		$str = Helper::template('global/modals');
		echo $str;
	}

	/**
	 * Get site title
	 *
	 * @return string
	 * @since 1.0.0
	 */
	public static function get_site_title(): string {
		return get_bloginfo('name');
	}

	/**
	 * Get site url
	 *
	 * @return string
	 * @since 1.0.0
	 */
	public static function get_site_url(): string {
		return site_url('/');
	}
	
	/**
	 * Throw dependency error
	 */
	public static function error( string $str, ?string $module = null ) {
		if ( $module ) {
			$module = "{$module}: &nbsp;";
		}
		Dependency::instance()->error(trim( "{$module} {$str}" ));
	}
	
}
<?php 
use MyThemeShop\Notification_Center;
use MyThemeShop\Notification;

/**
 * Handle dependency notice and errors
 * 
 * @since 1.0.0
 */
class Dependency {
	use Traits\Singleton;

	/**
	 * Instance of Notification_Center
	 *
	 * @var [type]
	 */
	protected  $Notification_Center = null;

	/**
	 * Init
	 */
	public function __construct(){
		$this->Notification_Center = new Notification_Center('dependency');
	}

	/**
	 * Error handler
	 *
	 * @param string $str
	 * @return void
	 */
	public function error( string $str ): void {
		if ( is_admin() ) {
			$this->Notification_Center->add( '<b>[Troop]</b> &nbsp; ' . $str, [
				'type' => Notification::ERROR
			]);
		}
	}
}
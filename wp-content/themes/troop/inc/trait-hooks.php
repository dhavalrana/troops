<?php 
namespace Traits;

/**
 * Wordpress hooks wrapper
 * 
 * * add_action
 * * add_filter
 * 
 * @version  1.0.0
 * @author Rohan Das
 */

trait Hooks {
	/**
	 * Wordpress add_action wrapper
	 *
	 * @param string $action
	 * @param string $method
	 * @param integer $priority
	 * @param integer $accepted_args
	 * @return void
	 */
	public function add_action( string  $action, string  $method, int $priority  = 11, int $accepted_args = 1 ) {
		add_action($action, [$this, $method], $priority, $accepted_args );
	}

	/**
	 * Wordpress add_filter wrapper
	 *
	 * @param string $action
	 * @param string $method
	 * @param integer $priority
	 * @param integer $accepted_args
	 * @return void
	 */
	public function add_filter( string  $action, string  $method, int $priority  = 11, int $accepted_args = 1 ) {	
		add_filter($action, [$this, $method], $priority, $accepted_args );
	}
}
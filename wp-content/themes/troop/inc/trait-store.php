<?php 
namespace Traits;

trait Store {
	protected $_store = [];

	/**
	 * Get value
	 *
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get(string $key, $default = null) {
		if ( isset( $this->_store[$key] ) ) {
			return $this->_store[$key];
		} else {
			if ( get_transient( $key ) !== false ) {
				$this->_store[$key] = get_transient( $key );
				return $this->_store[$key];
			}  else {
				return $default;
			}
		}
	}

	/**
	 * Set value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public function set(string $key, $value, $cache = null) {
		$this->_store[$key] = $value;
		if ( $cache ) {
			set_transient( $key, $value, $cache );
		}		
	}
}
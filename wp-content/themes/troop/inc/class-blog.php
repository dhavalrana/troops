<?php 

class Blog {	
	use Traits\Singleton;

	/**
	 * Init
	 */
	public function __construct() {
		add_action('acf/init', [$this, 'acf_fields']);
		add_action( 'pre_get_posts', [$this, 'set_custom_oder']);
	}


	/**
	 * Set custom posts order
	 * @return void
	 */
	public function set_custom_oder( $query ): void {
		if ( ! is_admin() && $query->is_main_query() ) {
			$order = filter_input(INPUT_GET, 'order', FILTER_SANITIZE_STRING );
			// var_dump($order);
			// exit;
			if ( $order ) {
				$query->set( 'order', strtoupper($order) );
			}

		}
	}


	/**
	 * Register acf fields
	 * @return void
	 */
	public function acf_fields(): void {
		Acfx::cpt_group('post', 'Banner', [
			Acfx::field('Image', 'Banner')->instructions('Blog banner background'),
			Acfx::field('Image', 'Thumbnail')->instructions('Blog card thumbnail'),
		]);
	}


	///////////////////////////////
	// STATIC FUNCTIONS
	///////////////////////////////

	/**
	 * Get blog title
	 * @return string
	 */
	public static function title(): string {
		return get_the_title();
	}
	/**
	 * Get blog publish date
	 * @return string
	 */
	public static function date(): string {
		return get_the_date('F jS, Y');
	}

	/**
	 * Get list of tags
	 * @return array
	 */
	public static function tags(): array {
		$list = [];
		$tags = get_the_tags( get_the_ID() );
		if ( $tags ) {
			foreach( $tags as $tag ) {
				$list[] = [
					'label' => $tag->name,
					'slug' 	=> $tag->slug,
					'url' 	=> get_tag_link( $tag ),
				];
			}
		}
		return $list;
	}

	/**
	 * Get list of tags
	 * @return array
	 */
	public static function all_tags(): array {
		$list = [];
		$tags = get_tags();
		if ( $tags ) {
			foreach( $tags as $tag ) {
				$list[] = [
					'label' => $tag->name,
					'slug' 	=> $tag->slug,
					'url' 	=> get_tag_link( $tag ),
				];
			}
		}
		return $list;
	}

	/**
	 * Get banner image url
	 * @return string
	 */
	public static function banner(): string {
		$val = field('banner');
		return Helper::acf_img( $val );
	}

	/**
	 * Get thumbnail image url
	 * @return string
	 */
	public static function thumbnail(): string {
		$val = field('thumbnail');
		return Helper::acf_img( $val );
	}

	/**
	 * Get permalink
	 * @return string
	 */
	public static function link(): string {
		return get_permalink();
	}

	/**
	 * Get blog excerpt
	 * @return string
	 */
	public static function excerpt() {
		$str = get_the_excerpt();
		return $str;
	}

	/**
	 * Related posts query
	 * @return array
	 */
	public static function related_posts(): array {
		$post_id = get_the_ID();
		$terms = wp_get_post_tags($post_id);
		$term_array = [];
		foreach( $terms as $term ) {
			$term_array[] = $term->slug;
		}

		$args = [
			'post_type' 		=> 'post',
			'post_status'	 	=> 'publish',
			'posts_per_page' 	=> 3,
			'tag' 				=> implode(",", $term_array),
			'exclude' 		=> $post_id,
		];
		$posts = get_posts( $args );
		return $posts;
	}

	/**
	 * Get blog page link
	 */
	public static function archive_link(): string {
		return get_permalink( get_option( 'page_for_posts' ) )?: '';
	}

	/**
	 * Get next page url
	 */
	public static function next_page_url(): string {
		$output = '';
		if ( $link = get_next_posts_link() ){
		preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $link, $result);			
			if (!empty($result)) {
				$output = $result['href'][0];
			}
		}
		return $output;
	}
 }
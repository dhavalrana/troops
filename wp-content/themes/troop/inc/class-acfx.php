<?php 
use WordPlate\Acf\Location;

class Acfx {
	use Traits\Singleton;

	public static $_module_key = '';

	///////////////////////////////
	// STATIC FUNCTIONS
	///////////////////////////////

	/**
	 * Create metabox
	 * @return void
	 */
	public static function cpt_group( string $post_type, string $title, array $fields = [] ): void {
		register_extended_field_group([
			'key' => sanitize_title( $post_type . $title ),
			'title' => $title,
			'label_placement' => 'left',
			'location' => [
				Location::if('post_type', $post_type)
			],
			'fields'=> $fields,
		]);
	}

	/**
	 * Create field
	 * @return mixed
	 */
	public static function field( string $type, string $label, ?string $key = null ) {
		if ( $key ) {
			$key = ( self::$_module_key ) ? self::$_module_key . '/' . $key : $key;
		}
		$class = "\\WordPlate\\Acf\\Fields\\{$type}";
		return $class::make( $label, $key );
	}

	/**
	 * set current module key
	 * @param string $module_key
	 * @return  void
	 */
	public static function set_module( string $module_key = '' ) {
		self::$_module_key = trim($module_key);
	}

	public static function reset(): void {
		self::$_module_key = '';
	}
}
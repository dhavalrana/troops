<?php 
namespace Traits;
/**
 * Singleton
 * 
 * @version  1.0.0
 * @author  Rohan Das
 */
trait Singleton {
	/**
	 * Save the instance
	 *
	 * @var [type]
	 */
	protected static $_instance = null;

	/**
	 * Get instance 
	 *
	 * @return void
	 */
	public static function instance() {
		if ( ! self::$_instance ) {
			self::$_instance = new self;
		}
		
		return self::$_instance;
	}

	/**
	 * Destroy instance
	 *
	 * @return void
	 */
	public static function destroy_instance() {
		if ( self::$_instance &&  \method_exists( self::$_instance, 'destroy' ) ) {
			\call_user_func([self::$_instance, 'destroy' ]);
		}
		self::$_instance = null;
	}
}
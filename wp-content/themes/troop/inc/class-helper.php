<?php 

/**
 * Helper functions
 * * is_dev: Check if its in dev mode
 * * template: Load template from template directory
 * * image: get image url relarive to asset dir
 * * acf_img: get image url from acf field value
 * * ph_img: Placeholder image URL generator
 * * default: toggle between value or default
 * * content: wpautop wrapper
 * 
 * 
 * @version  1.0.0
 * @author  Rohan Das
 */
class Helper {
	/**
	 * Check if its in dev mode
	 *
	 * @return boolean
	 * @since 1.0.0
	 */
	public static function is_dev(): bool {
		return ( defined('RD_DEV') && RD_DEV === true );
	}

	/**
	 * Load a template file
	 *
	 * @param string $file
	 * @param array|null $args
	 * @return string|null
	 * @since 1.0.0
	 */
	public static function load_template( string $file, ?array $args = [] ): ?string {
		$path = preg_replace("/\.php$/", '', $file) . '.php' ;
		$path = str_replace(RD_ROOT, '', $path);
		$located = locate_template( $path );
		
		if ( file_exists( $located ) ) {
			ob_start();		
			(function() use ( $args, $located ) {
				extract( $args );
				include( $located );
			})();
			return ob_get_clean();
		}
	}

	/**
	 * Load template from template directory
	 *
	 * @param string $file
	 * @param array $args
	 * @return string|null
	 * @since 1.0.0
	 */
	public static function template( string $file, array $args = [] ): ?string {
		$path =  RD_TEMPLATES . '/' . preg_replace("/\.php$/", '', $file) . '.php' ;
		return self::load_template( $path, $args );
	}


	/**
	 * Get image url from asset directory
	 *
	 * @param string $img
	 * @return string
	 * @since 1.0.0
	 */
	public static function img( string $img ): string {
		return RD_IMAGES_URI . '/' . $img;
	}

	/**
	 * Get image URL
	 *
	 * @param array|null $value
	 * @param string|null $size
	 * @return string
	 * @since 1.0.0
	 */
	public static function acf_img( $value, ?string $size = null ): string {
		$image_url = '';
		if ( $value && is_array($value) ) {
			$image_url = $value['url'];
			if ( $size && isset( $value['sizes'][$size] ) ) {
				$image_url = $value['sizes'][$size];
			}
		}
		return $image_url;
	}

	/**
	 * Placeholder image URL generator
	 *
	 * @param string $size
	 * @param string|null $text
	 * @return string
	 * @since 1.0.0
	 */
	public static function ph_img( string $size, ?string $text = null ): string {
		$url = "https://via.placeholder.com/{$size}.png";
		if ( $text ) {
			$text = urlencode ( $text );
			$url .= "?text={$text}";
		}
		return $url;
	}
	
	/**
	 * Check if value exists then return the value
	 * else return default
	 *
	 * @param mixed $val
	 * @param mixed $default
	 * @return mixed
	 * @since 1.0.0
	 */
	public static function default( $val = null, $default = null ) {
		if ( $val ) {
			// In case of array, check if its empty
			if ( is_array( $val ) ) {
				if ( ! empty( $val ) ) {
					return $val;
				}
			} 
			
			else {
				return $val;
			}
		} else {
			return $default;
		}
	}

	/**
	 * Add wpautop filter
	 *
	 * @param string $str
	 * @return string
	 */
	public static function content( ?string $str ): string {
		return wpautop( $str );
	}

	/**
	 * Extract href from <a> tag; this is to extract URL from wp next post link
	 *
	 * @param string $tag
	 * @return string
	 * @since 1.0.0
	 */
	public static function extract_link( string $tag ): string {
		$tag = self::default($tag, '');
		preg_match('/<a.*?href="(.*)".*?\/a>/', $tag, $match);
		$link =  isset( $match[1] ) ? $match[1] : '';
		return $link;
	}

	/**
	 * Parse css value
	 *
	 * @param string $value
	 * @return array
	 */
	public static function parse_value_value( $css ) {
		$result = [];
		$breakpoints = [
			'all' => '',
			'mobile' => 768,
			'tablet' => 1024, 
		];
		$breakpoints_keys = array_keys($breakpoints);
		$trim_map = function($item) { return trim($item); };
		$parts = array_map($trim_map, explode("|", $css));
		$exp = '/('.implode('|',$breakpoints_keys).')\s*:\s*(.*)/';
		foreach( $parts as $part ) {
			$match = Regex::match($exp, $part);
			$name = $match->hasMatch() ? $match->group(1) : $breakpoints_keys[0];
			$value = $match->hasMatch() ? $match->group(2) : trim($part);
			$result[] = [
				'name' => $name,
				'value' => $value,
				'query' => $breakpoints[$name],
			];
		}
		return $result;
	}
}
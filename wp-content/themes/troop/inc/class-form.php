<?php 
use WordPlate\Acf\Location;
/**
 * Form
 * 
 * @version 1.0.0
 * @author Rohan Das <rohan@kraftdesignsmiths.com>
 */
class Form {
	use Traits\Singleton;

	/**
	 * Init
	 */
	public function __construct() {
		if ( 
			   ! class_exists('Enqueue') 
			|| ! class_exists('Settings')  
			|| ! class_exists('Acfx')
		) {
			Core::error('Missing modules.', 'reCaptcha');	
			return;		
		}
		$this->create_settings();
		add_action('wp', [$this, 'load']);
		add_action('rest_api_init', [$this, 'endpoint_setup']);
	}

	/**
	 * Create settings
	 */
	public function create_settings(): void {
		Acfx::set_module('recaptcha');
		Settings::create_section('recaptcha', 'reCaptcha v3', [
			Acfx::field('TrueFalse', 'Enabled', 'enabled')->stylisedUi()->instructions('Enable or disable reCaptcha.'),
			Acfx::field('Text', 'Site Key', 'key')->instructions('Google reCaptcha v3. <a href="https://developers.google.com/recaptcha/docs/v3" target="_blank">Read more</a>'),
			Acfx::field('Text', 'Site Secret', 'secret')->instructions('Google reCaptcha v3. <a href="https://developers.google.com/recaptcha/docs/v3" target="_blank">Read more</a>'),
		]);
		Acfx::reset();
	}

	public function load(): void {
		if ( ! self::has_recaptcha() ) return;		
		Enqueue::js('recaptcha', 'https://www.google.com/recaptcha/api.js?render='.self::recaptcha_key());
		Enqueue::js_vars('recaptcha_site_key', self::recaptcha_key());
	}

	/**
	 * endpoint setup
	 */
	public function endpoint_setup(){
		register_rest_route('form', 'handler', [
			'methods' => 'POST',
			'callback' => [$this, 'form_handler'],
		]);
	}


	/**
	 * ajax handler
	 * @version 1.0.0
	 */
	public function form_handler() {
		if (  empty( $_POST ) ) {
			$_POST = json_decode(file_get_contents('php://input'), true); 
		}
		if ( self::has_recaptcha() ) {
			if ( ! $this->verify_token($_POST['token']) ) {
				$data = ['status'=>'fail', 'recaptcha' => 'failed'];
				header('Content-Type: application/json');
				echo json_encode($data);
				exit;
			}
		}

		$timestamp = date('Y-m-d H:i:s');


		if ($_POST['type'] == 'demo') {
			$to = "hello@trooptravel.com";
			$subject = "Booking Demo Request";
			
			$name = $_POST['name'];
			$email = $_POST['email'];
			$company = $_POST['company'];
			$country = $_POST['country'];
			$travel_budget = $_POST['travel_budget'];
			
			$email = "Name: $name\nEmail: $email\nCompany: $company\nCountry: $country\nTravel Budget: $travel_budget\n";
			
			@wp_mail($to, $subject, $email);
			
			$data['timestamp'] = $timestamp;
			$data['name'] = htmlspecialchars($_POST['name']);
			$data['email'] = htmlspecialchars($_POST['email']);
			$data['company'] = htmlspecialchars($_POST['company']);
			$data['country'] = htmlspecialchars($_POST['country']);
			$data['travel_budget'] = htmlspecialchars($_POST['travel_budget']);
			$handle = fopen( ABSPATH . '/protected/demo_data.csv', 'a');
			fputcsv($handle, $data);
			fclose($handle);			
			$data = ['status'=>'ok'];
			header('Content-Type: application/json');
			echo json_encode($data);
		} elseif($_POST['type'] == 'subscribe'){
			
			$data['timestamp'] = $timestamp;
			$data['email'] = htmlspecialchars($_POST['email']);
			$handle = fopen( ABSPATH . '/protected/subscribe_data.csv', 'a');
			fputcsv($handle, $data);
			fclose($handle);
			
			$data = ['status'=>'ok'];
			header('Content-Type: application/json');
			echo json_encode($data);
		} elseif($_POST['type'] == 'case-studies'){
			$data['timestamp'] = $timestamp;
			$data['email'] = htmlspecialchars($_POST['email']);
			$handle = fopen( ABSPATH . '/protected/case_studies.csv', 'a');
			fputcsv($handle, $data);
			fclose($handle);
		
			$to = "hello@trooptravel.com";
			$subject = "Case Studies Request";    
			$email = "Email: {$data['email']}\n";    
			@wp_mail($to, $subject, $email);
			
			$data = ['status'=>'ok'];
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = ['status'=>'fail', 'data' => $_POST];
			header('Content-Type: application/json');
			echo json_encode($data);
		}

		exit;
	}

	/**
	 * Verify token
	 */
	public function verify_token($token) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => self::recaptcha_secret(), 'response' => $token)));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$arrResponse = json_decode($response, true);
		return ($arrResponse["success"] == '1' && $arrResponse["action"] == 'submit' && $arrResponse["score"] >= 0.5);
	}


	/**
	 * Check if reCaptcha is enabled
	 */
	public static function has_recaptcha(){
		return (Settings::get('recaptcha/enabled') && Settings::get('recaptcha/key') && Settings::get('recaptcha/secret'));
	}

	/**
	 * Get reCaptcha key
	 */
	public static function recaptcha_key() {
		return Settings::get('recaptcha/key');
	}

	/**
	 * Get reCaptcha secret
	 */
	public static function recaptcha_secret() {
		return Settings::get('recaptcha/secret');
	}

	/**
	 * Get handler url
	 */
	public static function handler() {
		return home_url( '/wp-json/form/handler/' );
	}

}
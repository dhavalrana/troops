<?php 
use WordPlate\Acf\Location;
/**
 * Settings
 * 
 * @version 1.0.0
 * @author Rohan Das <rohan@kraftdesignsmiths.com>
 */
class Settings {
	use Traits\Singleton;

	/**
	 * Main settings page slug
	 * @var string
	 */
	const MENU_SLUG = 'troop-settings';

	/**
	 * Main settings page title
	 */
	const MENU_TITLE = 'Troop';

	/**
	 * Init
	 * * Create a base menu page
	 */
	public function __construct(){
		acf_add_options_page( [
			'page_title' 	=> self::MENU_TITLE,
			'menu_title'	=> self::MENU_TITLE,
			'menu_slug' 	=> self::MENU_SLUG,
			'capability'	=> 'manage_options',
			'redirect'		=> false,
			'icon_url'		=> Helper::img('admin/troop.png'),
		] );
		add_action( 'admin_menu', [$this, 'remove_empty_page'], 99 );
	}


	/**
	 * Remove empty page
	 */
	public function remove_empty_page(){
		global $menu, $submenu;
		unset($submenu['troop-settings'][0]);
	}

	/////////////////////////
	// Static functions
	/////////////////////////

	/**
	 * Create a new options section
	 * * Create a new sub menu page
	 * * Create new fields group
	 *
	 * @param string $key
	 * @param string $title
	 * @param array $fields
	 * @param int|null $order
	 * @return void
	 */
	public static function create_section( string $key, string $title, array $fields = [], ?int $order = 10 ): void {
		$slug = self::MENU_SLUG  . '-' . $key;

		acf_add_options_page( [
			'page_title' 	=> $title,
			'menu_title'	=> $title,
			'menu_slug' 	=> $slug,
			'capability'	=> 'manage_options',
			'parent_slug'	=> self::MENU_SLUG,
			'redirect'		=> false,
			// 'icon_url'		=> Helper::image('admin/cpt-property.png'),
			// 'icon_url'		=> 'dashicons-images-alt2',
		]);

		register_extended_field_group([
			'key'					=> $key,
			'title' 				=> $title,
			'fields' 				=> $fields,
			'style' 				=> 'default',
			'label_placement'		=> 'left',
			'location' 				=> [Location::if( 'options_page', $slug )],
			'menu_order' 			=> 20
		]);
	}

	/**
	 * Get option  value
	 *
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public static function get(  string $key, $default = null )  {
		$val = option( $key );
		return $val === null ? $default : $val;
	}
}
<?php 
// ENV MODE ======================================
define('RD_DEV', false );

// CREDENTIALS ===================================



// DO NOT CHANGE ==================================
define('RD_ROOT', get_template_directory());
define('RD_ROOT_URI', get_template_directory_uri());

define('RD_TEMPLATES', RD_ROOT . '/templates');
define('RD_BLOCKS', RD_ROOT . '/blocks');

define('RD_ASSETS_URI', RD_ROOT_URI . '/assets' );
define('RD_IMAGES_URI', RD_ASSETS_URI . '/images' );
define('RD_CSS_URI', RD_ASSETS_URI . '/css' );
define('RD_JS_URI', RD_ASSETS_URI . '/js' );
define('RD_LIB_URI', RD_ASSETS_URI . '/libs' );
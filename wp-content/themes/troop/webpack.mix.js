const mix = require('laravel-mix');
const purgecss = require('postcss-purgecss-laravel');
require('mix-tailwindcss');

mix.options({
	processCssUrls: false
});
mix.setPublicPath('assets');

mix.js('src/js/main.js', 'js');
mix.sass('src/scss/main.scss', 'css');
mix.sass('src/scss/main-v2.scss', 'css').tailwind()

// .options({
// // 	postCss: [
// // 		purgecss({
// // 			// enabled: true,
// // 			content: [
// // 				'./blocks/**/*.php',
// // 			]
// // 		}),
// // 	]
// // });

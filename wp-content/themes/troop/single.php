<?php 
get_header(); 
Core::navbar();
?>

<main class="page-main">
	<div class="section blog-hero has-overlay is-flex is-align-items-flex-end" style="--bg: url('<?= Blog::banner() ?>');">
		<div class="container">
			<a href="<?= Blog::archive_link(); ?>" class="back">
				<img src="<?= Helper::img('blog/icon-blog-back.svg') ?>" alt="">
			</a>
			<div class="title">
				<?= Blog::title() ?>
			</div>
		</div>					
	</div>

	<div class="section blog-info">
		<div class="container">
			<div class="columns is-align-items-center">
				<div class="column is-4 has-text-centered-mobile">
					<ul class="tags">
						<?php foreach( Blog::tags() as $tag ): ?>
						<li><a href="<?= $tag['url'] ?>"><?= $tag['label'] ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="column is-4 has-text-centered ">
					<div class="date"><?= Blog::date() ?></div>
				</div>
				<div class="column is-4 has-text-right has-text-centered-mobile">
					<ul class="social">
						<li><a href="#" data-url="<?= get_permalink(); ?>" data-sharer="facebook"><img src="<?= Helper::img('/blog/icon-fb.svg') ?>" alt=""></a></li>
						<li><a href="#" data-url="<?= get_permalink(); ?>" data-sharer="linkedin"><img src="<?= Helper::img('/blog/icon-linkedin.svg') ?>" alt=""></a></li>
						<li><a href="#" data-url="<?= get_permalink(); ?>" data-sharer="twitter" data-title="<?= get_the_title() ?>"><img src="<?= Helper::img('/blog/icon-twitter.svg') ?>" alt=""></a></li>
						<li><a href="#" data-url="<?= get_permalink(); ?>" data-copy class="tooltip"><img src="<?= Helper::img('/blog/icon-share.svg') ?>" alt=""> <span class="tooltip-text">Copied to clipboard</span> </a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section blog-content">
		<div class="container">
			<div class="content">
				<?php the_content() ?>
			</div>
		</div>
	</div>

	<?php if ( ! empty( $posts  = Blog::related_posts() ) ): ?>
	<div class="section blog-related">
		<div class="container">
			<div class="grid">
				<?php foreach( $posts as $post ): setup_postdata($post); ?>
				<div class="card">
					<div class="card-content">
						<div class="card-date">
							<?= Blog::date() ?>
						</div>

						<div class="card-title">
							<a href="<?= Blog::link() ?>"><?= Blog::title() ?></a>
						</div>
						<div class="card-excerpt">
							<?= Blog::excerpt() ?>
						</div>
						<ul class="card-tags">
							<?php foreach( Blog::tags() as $tag ): ?>
							<li><a href="<?= $tag['url'] ?>"><?= $tag['label'] ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>

				</div>
				<?php endforeach; wp_reset_postdata(); ?>

			</div>
		</div>
	</div>
	<?php endif ?>
</main>

<?php 
Core::footer();
Core::modals();
get_footer();

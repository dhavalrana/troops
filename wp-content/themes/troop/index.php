<?php 
get_header(); 
?>
<div class="page-container has-drawer">	
	<?php Core::navbar(); ?>

	<main class="page-main">
		<section class="section blog-header">
			<div class="container">
				<div class="columns">
					<div class="column is-8">
						<h1 class="title has-text-white is-spaced">
						<?php 
						if ( is_category() )  {
								echo single_cat_title('' , true );
						} else {
							echo "Blog";
						}

						?>
						</h1>
					</div>
					<div class="column is-4 is-flex is-justify-content-flex-end is-align-items-center">
						<form class="search" action="<?= Core::get_site_url() ?>" method="GET">
							<input type="text" placeholder="Search…" name="s">
							<button>
								<img src="<?= Helper::img('blog/icon-search.svg') ?>" alt="">
							</button>
						</form>
					</div>
				</div>
				<div class="is-flex is-justify-content-flex-end is-align-items-center">
					<div class="button-group">
						<div class="button-wrap">
							<div class="custom-select blog-sort">
								<select name="sort">
									<option selected="selected" value="" disabled="disabled">Sort</option>
									<option value="desc">Most recent</option>
									<option value="asc">Oldest</option>
								</select>
							</div>
						</div>
						<div class="button-wrap">
							<a href="#" data-drawer-open>
							<?php if ( is_tag() ): ?>
								<span style="text-decoration: none; display: inline-block; margin-right: 6px;">Tag: </span>
								<?php single_tag_title() ?>
							<?php else: ?>
								All Tags
							<?php endif ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section blog-masonry">
			<div class="container">
				<div class="masonry" data-next="<?= Blog::next_page_url() ?>">
					<?php while(have_posts()): the_post(); ?>
					<div class="masonry-item">
						<div class="card">
							<div class="card-image">
								<figure class="image">
									<img src="<?= Blog::thumbnail() ?>">
								</figure>
								<div class="card-date">
									<?= Blog::date() ?>
								</div>
							</div>
							<div class="card-content">
								<div class="card-title">
									<a href="<?= Blog::link() ?>"><?= Blog::title() ?></a>
								</div>
								<div class="card-excerpt">
									<?= Blog::excerpt() ?>
								</div>
								<ul class="card-tags">
									<?php foreach( Blog::tags() as $tag ): ?>
									<li><a href="<?= $tag['url'] ?>"><?= $tag['label'] ?></a></li>
									<?php endforeach ?>
								</ul>
							</div>

						</div>
					</div>
					<?php endwhile ?>
				</div>

				<!-- <div class="blog-loader">
					<figure>
						<img src="<?= Helper::img('blog/blog-loader.svg') ?>" alt="">
					</figure>
				</div> -->
			</div>
		</section>

		<?= Core::footer(); ?>
	</main>

	<div class="page-drawer">
		<div class="drawer-close" data-drawer-close>
			<img src="<?= Helper::img('blog/drawer-close.svg') ?>" alt="close">
		</div>

		<ul class="blog-tags">
			<?php if ( is_tag() ): ?>
			<li><a href="<?= Blog::archive_link() ?>">All Tags</a></li>
			<?php endif ?>
			<?php foreach(Blog::all_tags() as $tag): ?>
			<li><a href="<?= $tag['url'] ?>"><?= $tag['label'] ?></a></li>
			<?php endforeach ?>
		</ul>
	</div>

</div>
<?php 
Core::modals();
get_footer();

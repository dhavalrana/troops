<?php 

get_header(); 
Core::navbar();


if ( have_posts() ):
	while(have_posts()): 
		the_post();
		the_content();
	endwhile; 
endif;

Core::footer();
Core::modals();
get_footer();
<!-- FOOTER -->
<footer class="footer" id="contact">
    <div class="container">
        <div class="columns is-gapless">
            <div class="column is-4-desktop is-2-tablet has-text-centered-mobile">
                <ul class="site-map-links">
                    <li>
                        <a href="<?php echo site_url('about'); ?>">About</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('about#team'); ?>">The team</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('careers'); ?>">Careers</a>
                    </li>
                </ul>

            </div>
            <div class="column is-4-desktop is-5-tablet has-text-centered">
                <p class="has-text-white">Get in touch</p>
                <p class="get-in-touch is-size-5 has-text-weight-bold"><a
                        href="mailto:hello@trooptravel.com">hello@trooptravel.com</a></p>
                <div class="social-icons">
                    <!-- <a href="https://www.facebook.com/TroopTravel/" target="_blank">
                        <svg id="Group_779" data-name="Group 779" xmlns="http://www.w3.org/2000/svg" width="10.354"
                            height="22.432" viewBox="0 0 10.354 22.432">
                            <path id="Path_124" data-name="Path 124"
                                d="M387.026,158.465h4.517V147.154h3.152l.336-3.787h-3.488V141.21c0-.893.179-1.246,1.043-1.246h2.445v-3.93H391.9c-3.361,0-4.876,1.48-4.876,4.314v3.02h-2.35V147.2h2.35Z"
                                transform="translate(-384.676 -136.033)" fill="#fff" />
                        </svg>
                    </a> -->
                    <a href="https://www.linkedin.com/company/trooptravel" target="_blank">
                        <svg id="Group_778" data-name="Group 778" xmlns="http://www.w3.org/2000/svg" width="21.922"
                            height="21.931" viewBox="0 0 21.922 21.931">
                            <g id="Group_777" data-name="Group 777" transform="translate(0 0)">
                                <g id="Group_776" data-name="Group 776">
                                    <rect id="Rectangle_211" data-name="Rectangle 211" width="4.526" height="14.621"
                                        transform="translate(0.435 7.31)" fill="#fff" />
                                    <path id="Path_122" data-name="Path 122"
                                        d="M328.55,196.252a2.7,2.7,0,1,0-2.676-2.7A2.688,2.688,0,0,0,328.55,196.252Z"
                                        transform="translate(-325.874 -190.857)" fill="#fff" />
                                </g>
                                <path id="Path_123" data-name="Path 123"
                                    d="M337.05,204.222c0-2.055.946-3.28,2.757-3.28,1.665,0,2.465,1.176,2.465,3.28V211.9h4.5V202.64c0-3.916-2.22-5.81-5.321-5.81a5.1,5.1,0,0,0-4.406,2.415v-1.968h-4.341V211.9h4.341Z"
                                    transform="translate(-324.854 -189.966)" fill="#fff" />
                            </g>
                        </svg>
                    </a>
                    <a href="https://twitter.com/trooptravel" target="_blank">
                        <svg id="Group_775" data-name="Group 775" xmlns="http://www.w3.org/2000/svg" width="23.304"
                            height="18.938" viewBox="0 0 23.304 18.938">
                            <path id="Path_121" data-name="Path 121"
                                d="M269.007,156.086a13.6,13.6,0,0,0,20.919-12.074,9.693,9.693,0,0,0,2.385-2.475,9.536,9.536,0,0,1-2.745.753,4.787,4.787,0,0,0,2.1-2.645,9.592,9.592,0,0,1-3.035,1.161,4.785,4.785,0,0,0-8.147,4.361,13.569,13.569,0,0,1-9.854-5,4.786,4.786,0,0,0,1.479,6.383,4.766,4.766,0,0,1-2.166-.6,4.788,4.788,0,0,0,3.835,4.749,4.787,4.787,0,0,1-2.158.08,4.783,4.783,0,0,0,4.466,3.321A9.615,9.615,0,0,1,269.007,156.086Z"
                                transform="translate(-269.007 -139.296)" fill="#fff" />
                        </svg>
                    </a>
                    <!-- <a href="https://www.instagram.com/trooptravel/" target="_blank">
                        <svg id="Group_781" data-name="Group 781" xmlns="http://www.w3.org/2000/svg" width="21.373"
                            height="21.373" viewBox="0 0 21.373 21.373">
                            <g id="Group_780" data-name="Group 780" transform="translate(0 0)">
                                <path id="Path_126" data-name="Path 126"
                                    d="M445.972,137.1c-2.9,0-3.266.013-4.406.065a7.851,7.851,0,0,0-2.594.5,5.465,5.465,0,0,0-3.125,3.126,7.823,7.823,0,0,0-.5,2.594c-.052,1.14-.064,1.5-.064,4.406s.012,3.266.064,4.406a7.823,7.823,0,0,0,.5,2.594,5.465,5.465,0,0,0,3.125,3.126,7.88,7.88,0,0,0,2.594.5c1.14.051,1.5.063,4.406.063s3.266-.012,4.406-.063a7.88,7.88,0,0,0,2.594-.5,5.471,5.471,0,0,0,3.126-3.126,7.853,7.853,0,0,0,.5-2.594c.052-1.14.065-1.5.065-4.406s-.012-3.266-.065-4.406a7.853,7.853,0,0,0-.5-2.594,5.471,5.471,0,0,0-3.126-3.126,7.851,7.851,0,0,0-2.594-.5c-1.14-.051-1.5-.065-4.406-.065"
                                    transform="translate(-435.286 -137.101)" fill="#fff" />
                                <path id="Path_127" data-name="Path 127"
                                    d="M446.519,142.178a6.157,6.157,0,1,0,6.157,6.157,6.157,6.157,0,0,0-6.157-6.157m0,10.154a4,4,0,1,1,4-4,4,4,0,0,1-4,4"
                                    transform="translate(-435.833 -137.648)" fill="#0d152b" />
                                <path id="Path_128" data-name="Path 128"
                                    d="M452.916,142.153a1.439,1.439,0,1,1-1.439-1.439,1.439,1.439,0,0,1,1.439,1.439"
                                    transform="translate(-434.39 -137.866)" fill="#0d152b" />
                            </g>
                        </svg>
                    </a> -->
                    <a href="https://www.youtube.com/watch?v=0oaSpcamyD0&feature=youtu.be" target="_blank">
                        <svg id="Group_774" data-name="Group 774" xmlns="http://www.w3.org/2000/svg" width="24.846"
                            height="17.504" viewBox="0 0 24.846 17.504">
                            <g id="Group_773" data-name="Group 773" transform="translate(0 0)">
                                <path id="Path_118" data-name="Path 118"
                                    d="M343.638,301.313a3.122,3.122,0,0,0-2.2-2.21c-1.937-.523-9.707-.523-9.707-.523s-7.769,0-9.707.523a3.122,3.122,0,0,0-2.2,2.21,35.178,35.178,0,0,0,0,12.038,3.125,3.125,0,0,0,2.2,2.211c1.938.522,9.707.522,9.707.522s7.77,0,9.707-.522a3.124,3.124,0,0,0,2.2-2.211,35.176,35.176,0,0,0,0-12.038Z"
                                    transform="translate(-319.311 -298.58)" fill="#fff" />
                                <path id="Path_119" data-name="Path 119" d="M331.426,312.17v-7.388l6.494,3.694Z"
                                    transform="translate(-321.544 -299.723)" fill="#1e2945" />
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="column is-4-desktop  has-text-right-desktop has-text-centered-mobile">
                <p class="has-text-white">Sign up to to keep up with news from TROOP</p>
                <form name="subscribeForm" method="POST" action="<?= Form::handler() ?>" class="newsletter">
                    <input type="hidden" name="type" value="subscribe">
                    <div class="field has-addons">
                        <div class="control"><input class="input" required type="email" placeholder="Your email"
                                name="email" /></div>
                        <div class="control"><button type="submit"
                                class="button is-primary <?= Form::has_recaptcha() ? 'recaptcha-btn' : '' ?>">Sign
                                up</button></div>
                    </div>
                    <div class="thanks">Thank you!</div>
                </form>
            </div>
        </div>
    </div>
</footer>
<div class="sub-footer">
    <div class="container">
        <div class="columns is-gapless">
            <div class="column has-text-centered has-text-white-ter sub-footer-section">
                <p>
                    Copyright @ 2022 TROOP all rights reserved
                </p>
                <!-- <ul>
                    <li>
                        <a href="<?php echo site_url('terms'); ?>">Terms of use</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('privacy'); ?>">Privacy policy</a>
                    </li>
                </ul> -->
            </div>
        </div>
    </div>
</div>
<!-- ./FOOTER -->
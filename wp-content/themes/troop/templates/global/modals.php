<div id="book-a-demo-modal" class="book-a-demo-modal is-inactive">
    <div class="container">
        <div class="main-content">
            <div class="headings">
                <div class="columns is-gapless is-mobile">
                    <div class="column"><img src="<?= Helper::img('logo-icon.svg') ?>" alt="Troop Logo" /></div>
                    <div class="column has-text-right">
                        <button class="button is-white close-button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.971" height="16.971"
                                viewBox="0 0 16.971 16.971">
                                <g id="Cross_Large" data-name="Cross Large"
                                    transform="translate(43.841 -381.131) rotate(45)">
                                    <line id="Line_1" data-name="Line 1" y2="20" transform="translate(250.5 290.5)"
                                        fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                                    <line id="Line_2" data-name="Line 2" x1="20" transform="translate(240.5 300.5)"
                                        fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                                </g>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <form name="bookADemoForm" method="POST" action="<?= Form::handler() ?>">
                <input type="hidden" name="type" value="demo">
                <div class="columns is-multiline is-gapless">
                    <div class="column is-12">
                        <p class="subtitle is-size-5 has-text-weight-semibold">Book a demo and see how real-time data
                            can help you save time, money and even the planet.</p>
                    </div>
                    <div class="column is-12">
                        <div class="control has-floating-label">
                            <input class="input with-floating-label" id="demoName" placeholder=" " name="name"
                                required /> <label class="label is-floating-label" for="demoName">Your name &
                                surname*</label>
                        </div>
                    </div>
                    <div class="column is-12">
                        <div class="control has-floating-label">
                            <input class="input with-floating-label" id="demoEmail" placeholder=" " type="email"
                                name="email" required /> <label class="label is-floating-label"
                                for="demoEmail">Email*</label>
                        </div>
                    </div>
                    <div class="column is-12">
                        <div class="control has-floating-label">
                            <input class="input with-floating-label" id="demoCompany" placeholder=" " name="company"
                                required /> <label class="label is-floating-label" for="demoCompany">Company*</label>
                        </div>
                    </div>
                    <div class="column is-12">
                        <div class="control has-floating-label">
                            <input class="input with-floating-label" id="demoCountry" placeholder=" " name="country"
                                required /> <label class="label is-floating-label" for="demoCountry">Country of
                                headquarters*</label>
                        </div>
                    </div>
                    <div class="column is-12">
                        <div class="control">
                            <div class="custom-select">
                                <select required name="travel_budget">
                                    <option selected="selected" value="" disabled="disabled">Annual corporate travel
                                        budget*</option>
                                    <option value="up to $1 million">Up to $1 million</option>
                                    <option value="$1 million - $10 million">$1 million – $10 million</option>
                                    <option value="$10 million - $50 million">$10 million – $50 million</option>
                                    <option value="$50 million +">More than $50 million</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="column is-12"><button type="submit"
                            class="button is-primary <?= Form::has_recaptcha() ? 'recaptcha-btn' : '' ?>"
                            disabled="disabled">Send booking request</button></div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="book-a-demo-modal" class="case-study-modal is-inactive">
    <div class="main-content">
        <div class="headings">
            <div class="columns is-gapless is-mobile">
                <div class="column"><img src="<?= Helper::img('logo-icon.svg') ?>" alt="Troop Logo" /></div>
                <div class="column has-text-right">
                    <button class="button is-white close-button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16.971" height="16.971"
                            viewBox="0 0 16.971 16.971">
                            <g id="Cross_Large" data-name="Cross Large"
                                transform="translate(43.841 -381.131) rotate(45)">
                                <line id="Line_1" data-name="Line 1" y2="20" transform="translate(250.5 290.5)"
                                    fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                                <line id="Line_2" data-name="Line 2" x1="20" transform="translate(240.5 300.5)"
                                    fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                            </g>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <form name="caseAStudyForm" method="POST" action="<?= Form::handler() ?>">
            <div class="columns is-multiline is-gapless">
                <div class="column is-12">
                    <p class="subtitle is-size-5 has-text-weight-semibold">Please enter your details to download the
                        case study.</p>
                </div>
                <div class="column is-12">
                    <div class="control has-floating-label">
                        <input class="input with-floating-label" id="demoName" placeholder=" " name="name" required />
                        <label class="label is-floating-label" for="demoName">Your name & surname*</label>
                    </div>
                </div>
                <div class="column is-12">
                    <div class="control has-floating-label">
                        <input class="input with-floating-label" id="demoEmail" placeholder=" " type="email"
                            name="email" required /> <label class="label is-floating-label"
                            for="demoEmail">Email*</label>
                    </div>
                </div>
                <div class="column is-12">
                    <div class="control has-floating-label">
                        <input class="input with-floating-label" id="demoCompany" placeholder=" " name="company"
                            required /> <label class="label is-floating-label" for="demoCompany">Company*</label>
                    </div>
                </div>
                <div class="column is-12"><button type="submit"
                        class="button is-primary <?= Form::has_recaptcha() ? 'recaptcha-btn' : '' ?>"
                        disabled="disabled">Download case study</button></div>
            </div>
        </form>
    </div>
</div>
<div id="book-a-demo-modal" class="confirmation-modal is-inactive">
    <div class="container">
        <div class="main-content">
            <div class="headings">
                <div class="columns is-gapless is-mobile">
                    <div class="column"><img src="<?= Helper::img('logo-icon.svg') ?>" alt="Troop Logo" /></div>
                    <div class="column has-text-right">
                        <button class="button is-white close-button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.971" height="16.971"
                                viewBox="0 0 16.971 16.971">
                                <g id="Cross_Large" data-name="Cross Large"
                                    transform="translate(43.841 -381.131) rotate(45)">
                                    <line id="Line_1" data-name="Line 1" y2="20" transform="translate(250.5 290.5)"
                                        fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                                    <line id="Line_2" data-name="Line 2" x1="20" transform="translate(240.5 300.5)"
                                        fill="none" stroke="#0d152b" stroke-linecap="round" stroke-width="2" />
                                </g>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline is-gapless">
                <div class="column is-12">
                    <p class="subtitle is-size-5 has-text-weight-semibold"><span class="has-text-weight-bold">Thank
                            you!</span> Our team will be in touch soon.</p>
                </div>
            </div>
        </div>
    </div>
</div>
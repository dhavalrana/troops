<!-- Navbar -->
<header class="header page-header">
    <div class="container">
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="<?=Core::get_site_url()?>"><img src="<?=Helper::img('logo.svg')?>" /></a>
                <div class="navbar-burger" data-target="mainMenu">
                    Menu
                </div>
            </div>
            <div class="navbar-menu" id="mainMenu">
                <div class="navbar-burger" data-target="mainMenu">
                    &#10005;
                </div>
                <div class="navbar-start">
                    <a href="<?php echo site_url(); ?>/about" class="navbar-item">
                        About
                    </a>


                    <a href="<?php echo site_url(); ?>/careers" class="navbar-item">
                        Careers
                    </a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a href="#" class="navbar-link">
                            Resources
                        </a>

                        <div class="navbar-dropdown">
                            <!-- <a href="#" class="navbar-item">
                                Media
                            </a> -->

                            <a href="https://www.trooptravel.com/category/case-studies/" class="navbar-item">
                                Case studies
                            </a>

                            <!-- <a href="#" class="navbar-item">
                                White papers
                            </a> -->
                            <a href="<?php echo site_url(); ?>/blog" class="navbar-item">
                                Blog
                            </a>
                            <!-- <a href="#" class="navbar-item">
                                FAQ
                            </a> -->
                        </div>
                    </div>

                    <div class="menu-last-item"><a href="<?php echo site_url(); ?>/contact" class="navbar-item">Contact
                            us</a></div>
                </div>

            </div>
            <div class="navbar-end">
                <!-- <div class="navbar-item"><a href="<?php echo site_url(); ?>/category/case-studies/" class="">Case
                            Studies</a></div>
                    <div class="navbar-item"><a href="<?php echo site_url(); ?>/blog" class="">Blog</a></div> -->

                <div class="navbar-item"><button type="button" class="button is-primary book-a-demo-button">Book a
                        demo</button></div>
            </div>
        </nav>
    </div>
</header>
<!-- ./Navbar -->
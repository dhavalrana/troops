!(function (e) {
  function t(t) {
    for (
      var s, r, a = t[0], l = t[1], c = t[2], u = 0, p = [];
      u < a.length;
      u++
    )
      (r = a[u]),
        Object.prototype.hasOwnProperty.call(i, r) && i[r] && p.push(i[r][0]),
        (i[r] = 0);
    for (s in l) Object.prototype.hasOwnProperty.call(l, s) && (e[s] = l[s]);
    for (d && d(t); p.length; ) p.shift()();
    for (dd && d(t); p.length; ) p.shift()();
    return o.push.apply(o, c || []), n();
  }
  function n() {
    for (var e, t = 0; t < o.length; t++) {
      for (var n = o[t], s = !0, a = 1; a < n.length; a++) {
        var l = n[a];
        0 !== i[l] && (s = !1);
      }
      s && (o.splice(t--, 1), (e = r((r.s = n[0]))));
    }
    return e;
  }
  var s = {},
    i = {
      0: 0,
    },
    o = [];
  function r(t) {
    if (s[t]) return s[t].exports;
    var n = (s[t] = {
      i: t,
      l: !1,
      exports: {},
    });
    return e[t].call(n.exports, n, n.exports, r), (n.l = !0), n.exports;
  }
  (r.m = e),
    (r.c = s),
    (r.d = function (e, t, n) {
      r.o(e, t) ||
        Object.defineProperty(e, t, {
          enumerable: !0,
          get: n,
        });
    }),
    (r.dd = function (e, t, n) {
      r.o(e, t) ||
        Object.defineProperty(e, t, {
          enumerable: !0,
          get: n,
        });
    }),
    (r.r = function (e) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, {
          value: "Module",
        }),
        Object.defineProperty(e, "__esModule", {
          value: !0,
        });
    }),
    (r.t = function (e, t) {
      if ((1 & t && (e = r(e)), 8 & t)) return e;
      if (4 & t && "object" == typeof e && e && e.__esModule) return e;
      var n = Object.create(null);
      if (
        (r.r(n),
        Object.defineProperty(n, "default", {
          enumerable: !0,
          value: e,
        }),
        2 & t && "string" != typeof e)
      )
        for (var s in e)
          r.d(
            n,
            s,
            function (t) {
              return e[t];
            }.bind(null, s)
          );
      return n;
    }),
    (r.n = function (e) {
      var t =
        e && e.__esModule
          ? function () {
              return e.default;
            }
          : function () {
              return e;
            };
      return r.d(t, "a", t), t;
    }),
    (r.o = function (e, t) {
      return Object.prototype.hasOwnProperty.call(e, t);
    }),
    (r.p = "");
  var a = (window.webpackJsonp = window.webpackJsonp || []),
    l = a.push.bind(a);
  (a.push = t), (a = a.slice());
  for (var c = 0; c < a.length; c++) t(a[c]);
  var d = l;
  var dd = l;
  o.push([56, 1]), n();
})({
  56: function (e, t, n) {
    "use strict";
    n.r(t);
    n(57), n(68), n(69), n(70), n(72), n(74), n(75), n(77), n(80), n(97), n(99);
    var s = n(26),
      i = n.n(s);
    document.addEventListener("DOMContentLoaded", function () {
      var e,
        t,
        n = function (e) {
          return Array.prototype.slice.call(document.querySelectorAll(e), 0);
        },
        s = function (e) {
          return document.querySelector(e);
        },
        o = n(".navbar-burger");
      o.length > 0 &&
        o.forEach(function (e) {
          e.addEventListener("click", function () {
            var t = e.dataset.target,
              n = document.getElementById(t),
              s = document.querySelector("html");
            s && s.style && "hidden" === s.style.overflow
              ? (s.style.overflow = "unset")
              : (s.style.overflow = "hidden"),
              e.classList.toggle("is-active"),
              n.classList.toggle("is-active");
          });
        });
      var r = {};
      window.innerWidth < 1024
        ? ((e = n(".section")) &&
            e.length &&
            e.forEach(function (e) {
              (r.paddingLeft && "0px" !== r.paddingLeft) ||
                (r = e.currentStyle || window.getComputedStyle(e));
            }),
          (t = r.paddingLeft))
        : ((e = s(".header .container")),
          (t = (r = e.currentStyle || window.getComputedStyle(e)).marginLeft));
      var a = s(".home-clients .swiper-container");
      a &&
        new Swiper(a, {
          slidesPerView: "auto",
          spaceBetween: 23,
          centeredSlides: !0,
          allowTouchMove: !0,
          pagination: {
            el: ".swiper-pagination",
          },
          breakpoints: {
            807: {
              allowTouchMove: !1,
              centeredSlides: !1,
              slidesOffsetBefore: parseInt(t.slice(0, -2)) + 20,
              slidesOffsetAfter: parseInt(t.slice(0, -2)),
            },
          },
        });
      var l = s(".home-awards .swiper-container");
      l &&
        new Swiper(l, {
          slidesPerView: "auto",
          spaceBetween: 0,
          centeredSlides: !0,
          allowTouchMove: !0,
          loop: !0,
          breakpoints: {
            807: {
              allowTouchMove: !1,
              centeredSlides: !1,
              loop: !1,
              spaceBetween: 35,
              slidesOffsetBefore: parseInt(t.slice(0, -2)),
              slidesOffsetAfter: parseInt(t.slice(0, -2)),
            },
          },
        });
      var d = s(".home-features .swiper-container");
      d &&
        new Swiper(d, {
          slidesPerView: "auto",
          spaceBetween: 10,
          centeredSlides: !1,
          allowTouchMove: !0,
          containerModifierClass: "swiper-container",
          wrapperClass: "swiper-wrapper",
          slideClass: "feature-wrapper",
          autoHeight: !1,
          pagination: {
            el: ".swiper-pagination",
          },
          breakpoints: {
            600: {
              spaceBetween: 40,
            },
            807: {
              allowTouchMove: !1,
              spaceBetween: 0,
            },
          },
        });

      var c = s(".home-steps .swiper-container");
      c &&
        new Swiper(c, {
          slidesPerView: "auto",
          spaceBetween: 40,
          allowTouchMove: !0,
          centeredSlides: !0,
          pagination: {
            el: ".swiper-pagination",
          },
          breakpoints: {
            807: {
              allowTouchMove: !1,
              centeredSlides: !1,
              slidesOffsetBefore: parseInt(t.slice(0, -2)) + 40,
              slidesOffsetAfter: parseInt(t.slice(0, -2)),
            },
          },
        });
      var dd = s(".home-intro .swiper-container");
      dd &&
        new Swiper(dd, {
          slidesPerView: "auto",
          spaceBetween: 0,
          centeredSlides: !0,
          allowTouchMove: !0,
          loop: !1,
          containerModifierClass: "swiper-container",
          wrapperClass: "swiper-wrapper",
          slideClass: "intro-wrapper",
          breakpoints: {
            807: {
              allowTouchMove: !1,
              centeredSlides: !1,
              loop: !1,
            },
          },
        });
      var ll = s(".meet-team .swiper-container");
      ll &&
        new Swiper(ll, {
          slidesPerView: "auto",
          spaceBetween: 0,
          centeredSlides: !0,
          allowTouchMove: !0,
          loop: !1,
          containerModifierClass: "swiper-container",
          wrapperClass: "swiper-wrapper",
          slideClass: "swiper-slide",
          pagination: {
            el: ".swiper-pagination",
          },
          breakpoints: {
            807: {
              allowTouchMove: !1,
              centeredSlides: !1,
              loop: !1,
            },
          },
        });
      var lld = s(".our-team .swiper-container");
      lld &&
        new Swiper(lld, {
          slidesPerView: 2.5,
          spaceBetween: 0,
          centeredSlides: !1,
          allowTouchMove: !1,
          loop: !1,
          lazy: !0,
          containerModifierClass: "swiper-container",
          wrapperClass: "swiper-wrapper",
          slideClass: "swiper-slide",
          navigation: {
            nextEl: ".our-team .swiper-button-next",
            prevEl: ".our-team .swiper-button-prev",
          },
          slidesOffsetBefore: 32,
          slidesOffsetAfter: 32,
          breakpoints: {
            807: {
              slidesPerView: 4,
              allowTouchMove: !1,
              centeredSlides: !1,
              loop: !1,
              slidesOffsetBefore: 0,
              slidesOffsetAfter: 0,
            },
          },
        });
      var dl = s(".one-troop .swiper-container");
      dl &&
        new Swiper(dl, {
          slidesPerView: "auto",
          spaceBetween: 10,
          centeredSlides: !1,
          allowTouchMove: !0,
          containerModifierClass: "swiper-container",
          wrapperClass: "swiper-wrapper",
          slideClass: "feature-wrapper",
          autoHeight: !1,
          pagination: {
            el: ".swiper-pagination",
          },
          breakpoints: {
            600: {
              spaceBetween: 40,
            },
            807: {
              allowTouchMove: !1,
              spaceBetween: 0,
            },
          },
        });

      var u = document.querySelector("html");
      window.addEventListener("scroll", function () {
        document.documentElement.scrollTop > 500
          ? u.classList.add("has-small-nav")
          : u.classList.remove("has-small-nav");
      });
      var p = n(".book-a-demo-button"),
        f = s("#book-a-demo-modal"),
        m = s("#book-a-demo-modal .close-button");
      p &&
        p.length &&
        (p.map(function (e) {
          e.addEventListener("click", function () {
            f.classList.remove("is-inactive"), u.classList.add("is-clipped");
          });
        }),
        m.addEventListener("click", function () {
          f.classList.add("is-inactive"), u.classList.remove("is-clipped");
        }));
      var v = n(".case-study-button"),
        h = s(".case-study-modal"),
        g = s(".case-study-modal .close-button");
      v &&
        v.length &&
        (v.map(function (e) {
          e.addEventListener("click", function () {
            h.classList.remove("is-inactive"), u.classList.add("is-clipped");
          });
        }),
        g.addEventListener("click", function () {
          h.classList.add("is-inactive"), u.classList.remove("is-clipped");
        }));
      var w = s(".confirmation-modal"),
        b = s(".confirmation-modal .close-button"),
        L = function () {
          w.classList.remove("is-inactive"), u.classList.add("is-clipped");
        };
      b.addEventListener("click", function () {
        w.classList.add("is-inactive"), u.classList.remove("is-clipped");
      });

      var y = function (e) {
          return Object.values(e).reduce(function (e, t) {
            return (e[t.name] = t.value), e;
          }, {});
        },
        T = s(".book-a-demo-modal form"),
        S = s(".book-a-demo-modal form button"),
        E = new i.a(T, {
          classTo: "control",
          errorClass: "has-danger",
          successClass: "has",
          errorTextParent: "form-group",
          errorTextTag: "div",
          errorTextClass: "text-help",
        });
      T.addEventListener("change", function (e) {
        e.preventDefault(),
          E.validate() ? (S.disabled = !1) : (S.disabled = !0);
      }),
        T.addEventListener("submit", function (e) {
          e.preventDefault(),
            e.stopPropagation(),
            S.classList.add("is-loading");
          var t = y(e.target);
          fetch(T.getAttribute("action"), {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(t),
          })
            .then(function (e) {
              return e.json();
            })
            .then(function (e) {
              console.log(e), S.classList.remove("is-loading"), m.click(), L();
            });
        });
      /*Contact form START - mavenTeam*/
      if (s(".contact-us-form-wrap form")) {
        var cy = function (e) {
            return Object.values(e).reduce(function (e, t) {
              return (e[t.name] = t.value), e;
            }, {});
          },
          CT = s(".contact-us-form-wrap form"),
          CS = s(".contact-us-form-wrap form button"),
          CME = s("#contact-error-msg"),
          CMS = s("#contact-success-msg"),
          CTSL = s(".contact-us-form-wrap form .custom-select"),
          CE = new i.a(CT, {
            classTo: "control",
            errorClass: "has-danger",
            successClass: "has",
            errorTextParent: "form-group",
            errorTextTag: "div",
            errorTextClass: "text-help",
          });

        CTSL.addEventListener("click", function (e) {
          if (CT.getElementsByTagName("select")[0].selectedIndex > 0) {
            e.preventDefault(),
              CE.validate() ? (CS.disabled = !1) : (CS.disabled = !0);
          }
          /*CT.getElementsByClassName("select-selected")[0].onclick = function() {
					e.preventDefault(),CE.validate() ? (CS.disabled = !1) : (CS.disabled = !0);									
				};*/
        }),
          CT.addEventListener("change", function (e) {
            e.preventDefault(),
              CE.validate() ? (CS.disabled = !1) : (CS.disabled = !0);
          }),
          CT.addEventListener("submit", function (e) {
            e.preventDefault(),
              e.stopPropagation(),
              CS.classList.add("is-loading");
            var t = cy(e.target);
            fetch(CT.getAttribute("action"), {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(t),
            })
              .then(function (e) {
                return e.json();
              })
              .then(function (e) {
                //console.log(e), CS.classList.remove("is-loading"), m.click(), L();

                console.log(e),
                  CS.classList.remove("is-loading"),
                  CT.reset(),
                  (CS.disabled = 1);
                CT.getElementsByClassName("select-selected")[0].innerHTML =
                  CT.getElementsByTagName("select")[0].options[0].innerHTML;
                if (e.status == "fail") {
                  CME.style.display = "block";
                  CMS.style.display = "none";
                } else {
                  CME.style.display = "none";
                  CMS.style.display = "block";
                }
              });
          });
      }
      /*Contact form END*/
      var k,
        O,
        M,
        C,
        x,
        P,
        B,
        j,
        I,
        N = s(".case-study-modal form"),
        A = s(".case-study-modal form button"),
        D = new i.a(N, {
          classTo: "control",
          errorClass: "has-danger",
          successClass: "has",
          errorTextParent: "form-group",
          errorTextTag: "div",
          errorTextClass: "text-help",
        });
      for (
        N.addEventListener("change", function (e) {
          e.preventDefault(),
            D.validate() ? (A.disabled = !1) : (A.disabled = !0);
        }),
          N.addEventListener("submit", function (e) {
            e.preventDefault(),
              e.stopPropagation(),
              A.classList.add("is-loading");
            var t = y(e.target);
            fetch(N.getAttribute("action"), {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(t),
            })
              .then(function (e) {
                return e.json();
              })
              .then(function (e) {
                console.log(e),
                  A.classList.remove("is-loading"),
                  g.click(),
                  L();
              });
          }),
          C = (k = document.getElementsByClassName("custom-select")).length,
          O = 0;
        O < C;
        O++
      ) {
        for (
          x = (P = k[O].getElementsByTagName("select")[0]).length,
            (B = document.createElement("DIV")).setAttribute(
              "class",
              "select-selected"
            ),
            B.innerHTML = P.options[P.selectedIndex].innerHTML,
            k[O].appendChild(B),
            (j = document.createElement("DIV")).setAttribute(
              "class",
              "select-items select-hide"
            ),
            M = 1;
          M < x;
          M++
        )
          ((I = document.createElement("DIV")).innerHTML =
            P.options[M].innerHTML),
            I.addEventListener("click", function (e) {
              var t, n, s, i, o, r, a;
              for (
                r = (i =
                  this.parentNode.parentNode.getElementsByTagName("select")[0])
                  .length,
                  o = this.parentNode.previousSibling,
                  n = 0;
                n < r;
                n++
              )
                if (i.options[n].innerHTML == this.innerHTML) {
                  for (
                    i.selectedIndex = n,
                      o.innerHTML = this.innerHTML,
                      a = (t =
                        this.parentNode.getElementsByClassName(
                          "same-as-selected"
                        )).length,
                      s = 0;
                    s < a;
                    s++
                  )
                    t[s].removeAttribute("class");
                  this.setAttribute("class", "same-as-selected");
                  var event = new Event("change");
                  i.dispatchEvent(event);
                  break;
                }
              o.click();
            }),
            j.appendChild(I);
        k[O].appendChild(j),
          B.addEventListener("click", function (e) {
            e.stopPropagation(),
              H(this, !0),
              this.nextSibling.classList.toggle("select-hide"),
              this.classList.toggle("select-arrow-active");
          });
      }
      function H(e, t) {
        var n,
          s,
          i,
          o,
          r,
          a = [];
        for (
          n = document.getElementsByClassName("select-items"),
            s = document.getElementsByClassName("select-selected"),
            o = n.length,
            r = s.length,
            i = 0;
          i < r;
          i++
        )
          e == s[i] ? a.push(i) : s[i].classList.remove("select-arrow-active");
        for (i = 0; i < o; i++)
          a.indexOf(i) && n[i].classList.add("select-hide");
        t && (E.validate() ? (S.disabled = !1) : (S.disabled = !0));
      }
      document.addEventListener("click", H);
    });
  },
  99: function (e, t, n) {},
});

//# sourceMappingURL=app.59b28564.js.map

document.addEventListener("DOMContentLoaded", () => {
  var myVideo = document.getElementById("myVideo");
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add("is-active");
    myVideo.play();
  }

  function closeModal($el) {
    $el.classList.remove("is-active");
    myVideo.pause();
    myVideo.currentTime = 0;
  }

  function closeAllModals() {
    (document.querySelectorAll(".modal") || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll(".video-modal-button") || []).forEach(
    ($trigger) => {
      const modal = $trigger.dataset.target;
      const $target = document.getElementById(modal);
      console.log($target);

      $trigger.addEventListener("click", () => {
        openModal($target);
      });
    }
  );

  // Add a click event on various child elements to close the parent modal
  (
    document.querySelectorAll(
      ".modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button"
    ) || []
  ).forEach(($close) => {
    const $target = $close.closest(".modal");

    $close.addEventListener("click", () => {
      closeModal($target);
      myVideo.pause();
      myVideo.currentTime = 0;
    });
  });

  // Add a keyboard event to close all modals
  document.addEventListener("keydown", (event) => {
    const e = event || window.event;

    if (e.keyCode === 27) {
      // Escape key
      closeAllModals();
      myVideo.pause();
      myVideo.currentTime = 0;
    }
  });

  var seeMore = document.getElementById("seeMore");
  var extraThumbs = document.getElementsByClassName("extra-thumbs");
  seeMore.onclick = function () {
    seeMore.style.display = "none";
    // console.log(extraThumbs.length);

    for (i = 0; i < extraThumbs.length; i++) {
      extraThumbs[i].classList.add("show-thumb");
      // console.log(extraThumbs[i]);
    }
  };
});

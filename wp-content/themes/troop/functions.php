<?php 
// ini_set('display_errors', 1);
// error_reporting(E_ERROR | E_PARSE);

/**
 * Load composer
 */
require_once 'vendor/autoload.php';

/**
 * Load config
 */
require_once 'config.php';

/**
 * Load traits
 */
foreach(glob( RD_ROOT . '/inc/trait-*.php') as $file ) {
	require_once $file;
}

/**
 * Load classes
 */
foreach(glob( RD_ROOT . '/inc/class-*.php') as $file ) {
	require_once $file;
}

/**
 * Initiate theme functions
 */
if ( class_exists('Core') ) {
	Core::instance();
}
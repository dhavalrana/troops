<div class="bts-block">
	<!-- Banner -->
	<div class="bts-banner tw-bg-blue-oxford relative">
		<div class="container bts-container">
			<div class="content tw-text-white tw-text-center">			
				<h1>
					Welcome to Business Travel Show Europe 2021
				</h1>
				<h2>
					Thanks for visiting our stand!
				</h2>
				<p>
					Today organizing a meeting is hard. Planning includes many factors like travel restrictions, CO2 emissions, cost, visas, etc. Bringing all those data points together is a manual, time-consuming and inefficient process.  We have solved this!
				</p>
			</div>
		</div>
		<div class="bts-card-desktop">
			<ul class="card-nav">
				<li><a href="#" class="card-trigger" data-card="download">Download case studies</a></li>
				<li><a href="#" class="demo-trigger">Book a demo</a></li>
				<li><a href="#" class="card-trigger" data-card="contact">Connect with us</a></li>
			</ul>
			<div class="card-content" data-card="download">
				<h3 class="tw-text-40x50 tw-m-0 tw-mb-40 tw-text-center tw-text-blue-tint tw-font-bold">See how TROOP has helped others save big.</h3>
				<form action="<?= Form::handler() ?>" method="POST">
					<div class="tw-flex tw-mb-2 tw-justify-center tw-max-w-[550px] tw-mx-auto">
						<div class="field-wrap tw-flex-grow"><input type="email" name="email" placeholder="Email*" required /></div>
						<div class="field-wrap tw-ml-32 "><button type="submit" class="tw-w-180 <?= Form::has_recaptcha() ? 'recaptcha-btn' : '' ?>">Download</button></div>
					</div>
					<div class="field-agree text-center tw-mt-20 tw-flex tw-items-center tw-justify-center">
						<input type="radio" name="agree" id="agree" required>
						<label for="agree" class="tw-text-14x22 tw-ml-10 tw-text-blue-oxford">I agree to receive information and updates from TROOP. See our <a href="#" class="tw-text-current tw-underline">privacy policy</a>.</label>
					</div>
					<input type="hidden" name="type" value="case-studies">
					<div class="field-thanks">Thanks you!</div>
				</form>
			</div>

			<div class="card-content" data-card="contact">
				<h3 class="tw-text-40x50 tw-m-0 tw-mb-40 tw-text-center tw-text-blue-tint font-bold tw-font-bold">
						Keep the conversation going <br>
						<span class="tw-text-blue-x6">—  connect with our team.</span>
				</h3>
				<div class="tw-flex tw-max-w-[600px] tw-mx-auto">
					<div class="tw-flex tw-flex-col tw-justify-center tw-items-center tw-flex-grow tw-border-solid tw-border-0 tw-border-r-2 tw-border-blue-oxford">
						<div class="social tw-pr-50">
							<a href="https://www.facebook.com/TroopTravel/" target="_blank">
								<svg id="Group_779" data-name="Group 779" xmlns="http://www.w3.org/2000/svg" width="10.354" height="22.432" viewBox="0 0 10.354 22.432">
									<path id="Path_124" data-name="Path 124" d="M387.026,158.465h4.517V147.154h3.152l.336-3.787h-3.488V141.21c0-.893.179-1.246,1.043-1.246h2.445v-3.93H391.9c-3.361,0-4.876,1.48-4.876,4.314v3.02h-2.35V147.2h2.35Z" transform="translate(-384.676 -136.033)" fill="#0d152b"/>
								</svg>
							</a>

							<a href="https://www.linkedin.com/company/trooptravel" target="_blank">
								<svg id="Group_778" data-name="Group 778" xmlns="http://www.w3.org/2000/svg" width="21.922" height="21.931" viewBox="0 0 21.922 21.931">
									<g id="Group_777" data-name="Group 777" transform="translate(0 0)">
										<g id="Group_776" data-name="Group 776">
										<rect id="Rectangle_211" data-name="Rectangle 211" width="4.526" height="14.621" transform="translate(0.435 7.31)" fill="#0d152b"/>
										<path id="Path_122" data-name="Path 122" d="M328.55,196.252a2.7,2.7,0,1,0-2.676-2.7A2.688,2.688,0,0,0,328.55,196.252Z" transform="translate(-325.874 -190.857)" fill="#0d152b"/>
										</g>
										<path id="Path_123" data-name="Path 123" d="M337.05,204.222c0-2.055.946-3.28,2.757-3.28,1.665,0,2.465,1.176,2.465,3.28V211.9h4.5V202.64c0-3.916-2.22-5.81-5.321-5.81a5.1,5.1,0,0,0-4.406,2.415v-1.968h-4.341V211.9h4.341Z" transform="translate(-324.854 -189.966)" fill="#0d152b"/>
									</g>
								</svg>
							</a>
						
							<a href="https://twitter.com/trooptravel" target="_blank">
								<svg id="Group_775" data-name="Group 775" xmlns="http://www.w3.org/2000/svg" width="23.304" height="18.938" viewBox="0 0 23.304 18.938">
									<path id="Path_121" data-name="Path 121" d="M269.007,156.086a13.6,13.6,0,0,0,20.919-12.074,9.693,9.693,0,0,0,2.385-2.475,9.536,9.536,0,0,1-2.745.753,4.787,4.787,0,0,0,2.1-2.645,9.592,9.592,0,0,1-3.035,1.161,4.785,4.785,0,0,0-8.147,4.361,13.569,13.569,0,0,1-9.854-5,4.786,4.786,0,0,0,1.479,6.383,4.766,4.766,0,0,1-2.166-.6,4.788,4.788,0,0,0,3.835,4.749,4.787,4.787,0,0,1-2.158.08,4.783,4.783,0,0,0,4.466,3.321A9.615,9.615,0,0,1,269.007,156.086Z" transform="translate(-269.007 -139.296)" fill="#0d152b"/>
								</svg>
							</a>
						
							<a href="https://www.instagram.com/trooptravel/" target="_blank">
								<svg id="Group_781" data-name="Group 781" xmlns="http://www.w3.org/2000/svg" width="21.373" height="21.373" viewBox="0 0 21.373 21.373">
									<g id="Group_780" data-name="Group 780" transform="translate(0 0)">
										<path id="Path_126" data-name="Path 126" d="M445.972,137.1c-2.9,0-3.266.013-4.406.065a7.851,7.851,0,0,0-2.594.5,5.465,5.465,0,0,0-3.125,3.126,7.823,7.823,0,0,0-.5,2.594c-.052,1.14-.064,1.5-.064,4.406s.012,3.266.064,4.406a7.823,7.823,0,0,0,.5,2.594,5.465,5.465,0,0,0,3.125,3.126,7.88,7.88,0,0,0,2.594.5c1.14.051,1.5.063,4.406.063s3.266-.012,4.406-.063a7.88,7.88,0,0,0,2.594-.5,5.471,5.471,0,0,0,3.126-3.126,7.853,7.853,0,0,0,.5-2.594c.052-1.14.065-1.5.065-4.406s-.012-3.266-.065-4.406a7.853,7.853,0,0,0-.5-2.594,5.471,5.471,0,0,0-3.126-3.126,7.851,7.851,0,0,0-2.594-.5c-1.14-.051-1.5-.065-4.406-.065" transform="translate(-435.286 -137.101)" fill="#0d152b"/>
										<path id="Path_127" data-name="Path 127" d="M446.519,142.178a6.157,6.157,0,1,0,6.157,6.157,6.157,6.157,0,0,0-6.157-6.157m0,10.154a4,4,0,1,1,4-4,4,4,0,0,1-4,4" transform="translate(-435.833 -137.648)" fill="#fff"/>
										<path id="Path_128" data-name="Path 128" d="M452.916,142.153a1.439,1.439,0,1,1-1.439-1.439,1.439,1.439,0,0,1,1.439,1.439" transform="translate(-434.39 -137.866)" fill="#fff"/>
									</g>
								</svg>
							</a>
						
							<a href="https://www.youtube.com/watch?v=0oaSpcamyD0&feature=youtu.be" target="_blank">
								<svg id="Group_774" data-name="Group 774" xmlns="http://www.w3.org/2000/svg" width="24.846" height="17.504" viewBox="0 0 24.846 17.504">
									<g id="Group_773" data-name="Group 773" transform="translate(0 0)">
										<path id="Path_118" data-name="Path 118" d="M343.638,301.313a3.122,3.122,0,0,0-2.2-2.21c-1.937-.523-9.707-.523-9.707-.523s-7.769,0-9.707.523a3.122,3.122,0,0,0-2.2,2.21,35.178,35.178,0,0,0,0,12.038,3.125,3.125,0,0,0,2.2,2.211c1.938.522,9.707.522,9.707.522s7.77,0,9.707-.522a3.124,3.124,0,0,0,2.2-2.211,35.176,35.176,0,0,0,0-12.038Z" transform="translate(-319.311 -298.58)" fill="#0d152b"/>
										<path id="Path_119" data-name="Path 119" d="M331.426,312.17v-7.388l6.494,3.694Z" transform="translate(-321.544 -299.723)" fill="#fff"/>
									</g>
								</svg>
							</a>
							
						</div>
					</div>

					<div class="tw-flex tw-flex-col tw-justify-center tw-items-center tw-flex-grow">
						<div class="email tw-pl-50">
							<a href="mailto:hello@troptravel.com" class="tw-text-emerald-x1 tw-text-20x27 tw-underline">hello@troptravel.com</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="background">			
			<svg xmlns="http://www.w3.org/2000/svg" width="913.518" height="520.392" viewBox="0 0 913.518 520.392">
				<g id="Group_1022" data-name="Group 1022" transform="translate(-108.258 0)">
					<path id="Path_3818" data-name="Path 3818" d="M671.881,20.128h0a20.129,20.129,0,0,0-40.258,0h0a20.129,20.129,0,0,0,40.258,0ZM88.809,100.643h0a20.129,20.129,0,0,1-20.128,20.128H20.128A20.128,20.128,0,0,1,0,100.643H0A20.128,20.128,0,0,1,20.128,80.515H68.681A20.129,20.129,0,0,1,88.809,100.643Zm394.1,20.128H120.044a20.128,20.128,0,0,1-20.128-20.128h0a20.128,20.128,0,0,1,20.128-20.128h56.713a20.128,20.128,0,0,0,20.128-20.128h0a20.129,20.129,0,0,0-20.128-20.129H64.533A20.129,20.129,0,0,1,44.4,20.128h0A20.129,20.129,0,0,1,64.533,0h539.85a20.128,20.128,0,0,1,20.128,20.128h0a20.128,20.128,0,0,1-20.128,20.128H278.142a20.129,20.129,0,0,0-20.129,20.129h0a20.129,20.129,0,0,0,20.129,20.128h204.77a20.129,20.129,0,0,1,20.129,20.128h0A20.129,20.129,0,0,1,482.911,120.772Z" transform="translate(114.36 399.62)" fill="#1e2945"/>
					<path id="Path_3819" data-name="Path 3819" d="M820.027,80.515A20.129,20.129,0,0,0,799.9,100.643h0a20.129,20.129,0,1,0,40.258,0h0A20.129,20.129,0,0,0,820.027,80.515Zm26.023,20.128h0a20.129,20.129,0,0,1,20.128-20.128h0a20.129,20.129,0,0,1,20.129,20.128h0a20.129,20.129,0,0,1-20.129,20.129h0A20.129,20.129,0,0,1,846.05,100.643Zm-73.494,20.129H752.028a20.128,20.128,0,1,0,0,40.257h88.578a20.128,20.128,0,0,1,20.128,20.128h0a20.128,20.128,0,0,1-20.128,20.128H215.753a20.128,20.128,0,0,1-20.128-20.128h0a20.128,20.128,0,0,1,20.128-20.128H500.664a20.128,20.128,0,1,0,0-40.257H147.7a20.129,20.129,0,0,1-20.128-20.129h0A20.128,20.128,0,0,1,147.7,80.515h71.4a20.129,20.129,0,0,0,20.128-20.128h0a20.128,20.128,0,0,0-20.128-20.129H147.7a20.129,20.129,0,0,1-20.128-20.129h0A20.129,20.129,0,0,1,147.7,0H772.556a20.128,20.128,0,0,1,20.128,20.128h0a20.129,20.129,0,0,1-20.128,20.129H664.345a20.129,20.129,0,0,0-20.128,20.129h0a20.129,20.129,0,0,0,20.128,20.128h108.21a20.128,20.128,0,0,1,20.128,20.128h0A20.129,20.129,0,0,1,772.556,120.773Zm-700.435,0H20.128A20.129,20.129,0,0,1,0,100.643H0A20.129,20.129,0,0,1,20.128,80.515H72.121A20.129,20.129,0,0,1,92.25,100.643h0A20.129,20.129,0,0,1,72.121,120.773ZM893.39,40.258h-64.6a20.129,20.129,0,0,1-20.129-20.129h0A20.129,20.129,0,0,1,828.787,0h64.6a20.129,20.129,0,0,1,20.129,20.128h0A20.129,20.129,0,0,1,893.39,40.258Z" transform="translate(108.258 77.561)" fill="#1e2945"/>
					<path id="Path_3820" data-name="Path 3820" d="M480.732,80.515h-258.5a20.129,20.129,0,0,0,0,40.258H146.52a20.129,20.129,0,0,0,0-40.258H19.488l-.01-.016A20.125,20.125,0,0,1,0,60.386H0A20.129,20.129,0,0,1,20.128,40.258H43.973A20.129,20.129,0,0,0,64.1,20.129h0A20.129,20.129,0,0,0,43.973,0h178.26A20.129,20.129,0,0,0,202.1,20.129h0a20.129,20.129,0,0,0,20.128,20.128H323.072A20.128,20.128,0,0,0,343.2,20.129h0A20.129,20.129,0,0,0,323.072,0H440.463a20.129,20.129,0,0,0-20.129,20.129h0a20.129,20.129,0,0,0,20.129,20.128h40.269a20.129,20.129,0,0,1,20.129,20.128h0A20.128,20.128,0,0,1,480.732,80.515Zm131.348,0H531.5a20.129,20.129,0,0,1-20.129-20.128h0A20.129,20.129,0,0,1,531.5,40.258H612.08a20.129,20.129,0,0,1,20.128,20.128h0A20.128,20.128,0,0,1,612.08,80.515Z" transform="translate(758.241 399.62) rotate(180)" fill="#1e2945"/>
					<path id="Path_3822" data-name="Path 3822" d="M88.809,20.128h0A20.129,20.129,0,0,0,68.68,0H20.129A20.128,20.128,0,0,0,0,20.128H0A20.128,20.128,0,0,0,20.129,40.257H68.68A20.129,20.129,0,0,0,88.809,20.128Zm32.4,20.128h0a20.129,20.129,0,0,1-20.129-20.128h0A20.129,20.129,0,0,1,121.206,0h0a20.129,20.129,0,0,1,20.129,20.128h0A20.129,20.129,0,0,1,121.206,40.257Z" transform="translate(640.692 0)" fill="#1e2945"/>
				</g>
			</svg>
		</div>
	</div>

	<div class="bts-card-mobile">
		<div class="swiper">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="card-content">
						<h2>DOWNLOAD CASE STUDIES</h2>
						<h3>See how TROOP has helped others save big.</h3>
						<form action="<?= Form::handler() ?>" method="POST">
							<div class="fields">
								<input type="email" name="email" placeholder="Email*" />
								<button type="submit" class="btn <?= Form::has_recaptcha() ? 'recaptcha-btn' : '' ?>">Download</button>
							</div>
							<div class="field-agree text-center tw-mt-20 tw-flex tw-items-center tw-justify-center">
								<input type="radio" name="agree" id="agree2">
								<label for="agree2" class="">I agree to receive information and updates from TROOP. See our <a href="#" class="tw-text-current tw-underline">privacy policy</a>.</label>
							</div>
							<input type="hidden" name="type" value="case-studies">
							<div class="field-thanks">Thanks you!</div>
						</form>
					</div>
				</div> 
				<div class="swiper-slide">
					<div class="card-content">
						<h2>BOOK A DEMO</h2>
						<h3>Take TROOP for a test drive and see for yourself.</h3>
						<p class="btn-wrap">
							<a href="#" class="btn demo-trigger">Book a demo</a>
						</p>
						<p>
							Our team will follow up with more details as soon as we receive your message.
						</p>
					</div>
				</div>
				
				<div class="swiper-slide">
					<div class="card-content">
						<h2>CONNECT WITH US</h2>
						<h3>Keep the conversation going <span>— connect with our team.</span></h3>
						
						<div class="social">
							<a href="https://www.facebook.com/TroopTravel/" target="_blank">
								<svg id="Group_779" data-name="Group 779" xmlns="http://www.w3.org/2000/svg" width="10.354" height="22.432" viewBox="0 0 10.354 22.432">
									<path id="Path_124" data-name="Path 124" d="M387.026,158.465h4.517V147.154h3.152l.336-3.787h-3.488V141.21c0-.893.179-1.246,1.043-1.246h2.445v-3.93H391.9c-3.361,0-4.876,1.48-4.876,4.314v3.02h-2.35V147.2h2.35Z" transform="translate(-384.676 -136.033)" fill="#0d152b"/>
								</svg>
							</a>

							<a href="https://www.linkedin.com/company/trooptravel" target="_blank">
								<svg id="Group_778" data-name="Group 778" xmlns="http://www.w3.org/2000/svg" width="21.922" height="21.931" viewBox="0 0 21.922 21.931">
									<g id="Group_777" data-name="Group 777" transform="translate(0 0)">
										<g id="Group_776" data-name="Group 776">
										<rect id="Rectangle_211" data-name="Rectangle 211" width="4.526" height="14.621" transform="translate(0.435 7.31)" fill="#0d152b"/>
										<path id="Path_122" data-name="Path 122" d="M328.55,196.252a2.7,2.7,0,1,0-2.676-2.7A2.688,2.688,0,0,0,328.55,196.252Z" transform="translate(-325.874 -190.857)" fill="#0d152b"/>
										</g>
										<path id="Path_123" data-name="Path 123" d="M337.05,204.222c0-2.055.946-3.28,2.757-3.28,1.665,0,2.465,1.176,2.465,3.28V211.9h4.5V202.64c0-3.916-2.22-5.81-5.321-5.81a5.1,5.1,0,0,0-4.406,2.415v-1.968h-4.341V211.9h4.341Z" transform="translate(-324.854 -189.966)" fill="#0d152b"/>
									</g>
								</svg>
							</a>
						
							<a href="https://twitter.com/trooptravel" target="_blank">
								<svg id="Group_775" data-name="Group 775" xmlns="http://www.w3.org/2000/svg" width="23.304" height="18.938" viewBox="0 0 23.304 18.938">
									<path id="Path_121" data-name="Path 121" d="M269.007,156.086a13.6,13.6,0,0,0,20.919-12.074,9.693,9.693,0,0,0,2.385-2.475,9.536,9.536,0,0,1-2.745.753,4.787,4.787,0,0,0,2.1-2.645,9.592,9.592,0,0,1-3.035,1.161,4.785,4.785,0,0,0-8.147,4.361,13.569,13.569,0,0,1-9.854-5,4.786,4.786,0,0,0,1.479,6.383,4.766,4.766,0,0,1-2.166-.6,4.788,4.788,0,0,0,3.835,4.749,4.787,4.787,0,0,1-2.158.08,4.783,4.783,0,0,0,4.466,3.321A9.615,9.615,0,0,1,269.007,156.086Z" transform="translate(-269.007 -139.296)" fill="#0d152b"/>
								</svg>
							</a>
						
							<a href="https://www.instagram.com/trooptravel/" target="_blank">
								<svg id="Group_781" data-name="Group 781" xmlns="http://www.w3.org/2000/svg" width="21.373" height="21.373" viewBox="0 0 21.373 21.373">
									<g id="Group_780" data-name="Group 780" transform="translate(0 0)">
										<path id="Path_126" data-name="Path 126" d="M445.972,137.1c-2.9,0-3.266.013-4.406.065a7.851,7.851,0,0,0-2.594.5,5.465,5.465,0,0,0-3.125,3.126,7.823,7.823,0,0,0-.5,2.594c-.052,1.14-.064,1.5-.064,4.406s.012,3.266.064,4.406a7.823,7.823,0,0,0,.5,2.594,5.465,5.465,0,0,0,3.125,3.126,7.88,7.88,0,0,0,2.594.5c1.14.051,1.5.063,4.406.063s3.266-.012,4.406-.063a7.88,7.88,0,0,0,2.594-.5,5.471,5.471,0,0,0,3.126-3.126,7.853,7.853,0,0,0,.5-2.594c.052-1.14.065-1.5.065-4.406s-.012-3.266-.065-4.406a7.853,7.853,0,0,0-.5-2.594,5.471,5.471,0,0,0-3.126-3.126,7.851,7.851,0,0,0-2.594-.5c-1.14-.051-1.5-.065-4.406-.065" transform="translate(-435.286 -137.101)" fill="#0d152b"/>
										<path id="Path_127" data-name="Path 127" d="M446.519,142.178a6.157,6.157,0,1,0,6.157,6.157,6.157,6.157,0,0,0-6.157-6.157m0,10.154a4,4,0,1,1,4-4,4,4,0,0,1-4,4" transform="translate(-435.833 -137.648)" fill="#fff"/>
										<path id="Path_128" data-name="Path 128" d="M452.916,142.153a1.439,1.439,0,1,1-1.439-1.439,1.439,1.439,0,0,1,1.439,1.439" transform="translate(-434.39 -137.866)" fill="#fff"/>
									</g>
								</svg>
							</a>
						
							<a href="https://www.youtube.com/watch?v=0oaSpcamyD0&feature=youtu.be" target="_blank">
								<svg id="Group_774" data-name="Group 774" xmlns="http://www.w3.org/2000/svg" width="24.846" height="17.504" viewBox="0 0 24.846 17.504">
									<g id="Group_773" data-name="Group 773" transform="translate(0 0)">
										<path id="Path_118" data-name="Path 118" d="M343.638,301.313a3.122,3.122,0,0,0-2.2-2.21c-1.937-.523-9.707-.523-9.707-.523s-7.769,0-9.707.523a3.122,3.122,0,0,0-2.2,2.21,35.178,35.178,0,0,0,0,12.038,3.125,3.125,0,0,0,2.2,2.211c1.938.522,9.707.522,9.707.522s7.77,0,9.707-.522a3.124,3.124,0,0,0,2.2-2.211,35.176,35.176,0,0,0,0-12.038Z" transform="translate(-319.311 -298.58)" fill="#0d152b"/>
										<path id="Path_119" data-name="Path 119" d="M331.426,312.17v-7.388l6.494,3.694Z" transform="translate(-321.544 -299.723)" fill="#fff"/>
									</g>
								</svg>
							</a>
							
						</div>

						<div class="email">
							<a href="mailto:hello@troptravel.com" class="">hello@troptravel.com</a>
						</div>

					</div>
				</div>
			</div>
			<div class="swiper-pagination"></div>
		</div>
	</div>



	<!-- VIDEO -->
	<div class="bts-video tw-bg-blue-tint">
		<div class="container bts-container">
			<div class="video-wrap">
				<video controls poster="/wp-content/uploads/2021/09/210928_Troop_animation_poster.jpg">
					<source src="/wp-content/uploads/2021/09/210928_Troop_animation.mp4" type="video/mp4">
				</video>
			</div>
		</div>

		<div class="background">
			<svg xmlns="http://www.w3.org/2000/svg" width="852" height="72.843" viewBox="0 0 852 72.843">
				<g id="Group_1170" data-name="Group 1170" transform="translate(-476 -799.625)">
					<path id="Path_3823" data-name="Path 3823" d="M737.011,107.5h0a18.117,18.117,0,0,1-18.118,18.117h-43.7A18.117,18.117,0,0,1,657.076,107.5h0a18.117,18.117,0,0,1,18.117-18.117h43.7A18.117,18.117,0,0,1,737.011,107.5Zm29.159-18.117h0A18.117,18.117,0,0,0,748.053,107.5h0a18.117,18.117,0,0,0,18.118,18.117h0A18.117,18.117,0,0,0,784.288,107.5h0A18.117,18.117,0,0,0,766.171,89.378ZM558.854,143.729h0a18.118,18.118,0,0,1,18.118-18.117h47.1A18.117,18.117,0,0,0,642.191,107.5h0a18.117,18.117,0,0,0-18.118-18.117H99.977A18.117,18.117,0,0,0,81.86,107.5h0a18.117,18.117,0,0,0,18.117,18.117H497.238a18.117,18.117,0,0,1,18.117,18.117h0a18.117,18.117,0,0,1-18.117,18.118h79.734A18.118,18.118,0,0,1,558.854,143.729Z" transform="translate(1260.288 961.472) rotate(180)" fill="#0d152b"/>
					<path id="Path_3825" data-name="Path 3825" d="M909.631,107.5h0a18.117,18.117,0,0,1-18.118,18.117h-43.7A18.117,18.117,0,0,1,829.7,107.5h0a18.117,18.117,0,0,1,18.117-18.117h43.7A18.117,18.117,0,0,1,909.631,107.5ZM938.79,89.378h0A18.117,18.117,0,0,0,920.672,107.5h0a18.117,18.117,0,0,0,18.118,18.117h0A18.117,18.117,0,0,0,956.908,107.5h0A18.117,18.117,0,0,0,938.79,89.378Z" transform="translate(371.092 746.855)" fill="#0d152b"/>
				</g>
			</svg>
		</div>
	</div>

	<!-- PEOPLE -->
	<div class="bts-people">
		<div class="container bts-container">
			<h3>Did we meet at the show?</h3>
			<h2>Let’s stay in touch!</h2>

			<div class="people">
				<a class="pbox" style="--tint:#0021C7;" href="https://www.linkedin.com/in/dennis-vilovic/" target="_blank">
					<div class="pbox-photo"><img src="/wp-content/uploads/2021/09/dennis-photo.png" /></div>
					<div class="pbox-photo-mob"><img src="/wp-content/uploads/2021/09/dennis-photo-mob.png" /></div>
					<div class="pbox-content">
						<h2>Dennis<br>Vilovic</h2>
						<h4>Founder</h4>
					</div>

					<span class="linkedin">
						<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
							<path id="Path_9113" data-name="Path 9113" d="M338.009,3.03H303.923a2.974,2.974,0,0,0-3.016,2.88V40.15a2.587,2.587,0,0,0,2.54,2.88h34.086c1.631,0,3.374-1.292,3.374-2.88V5.91A2.869,2.869,0,0,0,338.009,3.03ZM312.337,37.314H306.62V18.267h5.716Zm-2.858-22.14a3.572,3.572,0,1,1,3.572-3.572A3.57,3.57,0,0,1,309.479,15.174Zm25.712,22.14h-5.713V27.787c0-2.534-1.01-4.759-3.377-4.759-2.871,0-4.24,1.947-4.24,5.139v9.146h-5.716V18.267h5.386v2.747h.059c.822-1.48,3.248-2.985,6.245-2.985,5.754,0,7.357,3.056,7.357,8.714Z" transform="translate(-300.907 -3.03)" fill="#fff"/>
						</svg>
					</span>
				</a>
				<a class="pbox" style="--tint:#03D07D;" href="https://www.linkedin.com/in/fionapichavant/" target="_blank" >
					<div class="pbox-photo"><img src="/wp-content/uploads/2021/09/fiona-photo.png" /></div>
					<div class="pbox-photo-mob"><img src="/wp-content/uploads/2021/09/fiona-photo-mob.png" /></div>
					<div class="pbox-content">
						<h2>Fiona<br>Pichavant</h2>
						<h4>Customer Success</h4>
					</div>
					<span class="linkedin">
						<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
							<path id="Path_9113" data-name="Path 9113" d="M338.009,3.03H303.923a2.974,2.974,0,0,0-3.016,2.88V40.15a2.587,2.587,0,0,0,2.54,2.88h34.086c1.631,0,3.374-1.292,3.374-2.88V5.91A2.869,2.869,0,0,0,338.009,3.03ZM312.337,37.314H306.62V18.267h5.716Zm-2.858-22.14a3.572,3.572,0,1,1,3.572-3.572A3.57,3.57,0,0,1,309.479,15.174Zm25.712,22.14h-5.713V27.787c0-2.534-1.01-4.759-3.377-4.759-2.871,0-4.24,1.947-4.24,5.139v9.146h-5.716V18.267h5.386v2.747h.059c.822-1.48,3.248-2.985,6.245-2.985,5.754,0,7.357,3.056,7.357,8.714Z" transform="translate(-300.907 -3.03)" fill="#fff"/>
						</svg>
					</span>
				</a>
				<a class="pbox" style="--tint:#F752D5;" href="https://www.linkedin.com/in/spencer-brace/" target="_blank">
					<div class="pbox-photo"><img src="/wp-content/uploads/2021/09/spencer-photo.png" /></div>
					<div class="pbox-photo-mob"><img src="/wp-content/uploads/2021/09/spencer-photo-mob.png" /></div>
					<div class="pbox-content">
						<h2>Spencer<br>Brace</h2>
						<h4>Partnerships</h4>
					</div>
					<span class="linkedin">
						<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
							<path id="Path_9113" data-name="Path 9113" d="M338.009,3.03H303.923a2.974,2.974,0,0,0-3.016,2.88V40.15a2.587,2.587,0,0,0,2.54,2.88h34.086c1.631,0,3.374-1.292,3.374-2.88V5.91A2.869,2.869,0,0,0,338.009,3.03ZM312.337,37.314H306.62V18.267h5.716Zm-2.858-22.14a3.572,3.572,0,1,1,3.572-3.572A3.57,3.57,0,0,1,309.479,15.174Zm25.712,22.14h-5.713V27.787c0-2.534-1.01-4.759-3.377-4.759-2.871,0-4.24,1.947-4.24,5.139v9.146h-5.716V18.267h5.386v2.747h.059c.822-1.48,3.248-2.985,6.245-2.985,5.754,0,7.357,3.056,7.357,8.714Z" transform="translate(-300.907 -3.03)" fill="#fff"/>
						</svg>
					</span>
				</a>
				<a class="pbox" style="--tint:#FAC312;" href="https://www.linkedin.com/in/justinruane/" target="_blank">
					<div class="pbox-photo"><img src="/wp-content/uploads/2021/09/justin-photo.png" /></div>
					<div class="pbox-photo-mob"><img src="/wp-content/uploads/2021/09/justin-photo-mob.png" /></div>
					<div class="pbox-content">
						<h2>Justin<br>Ruane</h2>
						<h4>Sales</h4>
					</div>
					<span  class="linkedin">
						<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
							<path id="Path_9113" data-name="Path 9113" d="M338.009,3.03H303.923a2.974,2.974,0,0,0-3.016,2.88V40.15a2.587,2.587,0,0,0,2.54,2.88h34.086c1.631,0,3.374-1.292,3.374-2.88V5.91A2.869,2.869,0,0,0,338.009,3.03ZM312.337,37.314H306.62V18.267h5.716Zm-2.858-22.14a3.572,3.572,0,1,1,3.572-3.572A3.57,3.57,0,0,1,309.479,15.174Zm25.712,22.14h-5.713V27.787c0-2.534-1.01-4.759-3.377-4.759-2.871,0-4.24,1.947-4.24,5.139v9.146h-5.716V18.267h5.386v2.747h.059c.822-1.48,3.248-2.985,6.245-2.985,5.754,0,7.357,3.056,7.357,8.714Z" transform="translate(-300.907 -3.03)" fill="#fff"/>
						</svg>
					</span>
				</a>
			</div>
		</div>
		<div class="background">
			<svg xmlns="http://www.w3.org/2000/svg" width="604.743" height="108.704" viewBox="0 0 604.743 108.704">
				<path id="Path_3824" data-name="Path 3824" d="M886.434,274.181h0a18.118,18.118,0,0,1-36.235,0h0a18.118,18.118,0,0,1,36.235,0ZM361.626,201.712h0A18.117,18.117,0,0,0,343.509,183.6h-43.7a18.117,18.117,0,0,0-18.117,18.117h0a18.117,18.117,0,0,0,18.117,18.117h43.7A18.117,18.117,0,0,0,361.626,201.712ZM716.348,183.6H389.739a18.117,18.117,0,0,0-18.117,18.117h0a18.117,18.117,0,0,0,18.117,18.117h51.046A18.117,18.117,0,0,1,458.9,237.946h0a18.117,18.117,0,0,1-18.117,18.118H339.776a18.117,18.117,0,0,0-18.118,18.117h0A18.117,18.117,0,0,0,339.776,292.3H825.682A18.117,18.117,0,0,0,843.8,274.181h0a18.117,18.117,0,0,0-18.117-18.117H532.04a18.118,18.118,0,0,1-18.117-18.118h0a18.118,18.118,0,0,1,18.117-18.117H716.348a18.117,18.117,0,0,0,18.118-18.117h0A18.117,18.117,0,0,0,716.348,183.6Z" transform="translate(886.434 292.299) rotate(180)" fill="#0d152b"/>
			</svg>
		</div>
	</div>


	<!-- BOTTOM -->
	<div class="bts-bottom">
		<div class="container bts-container">
			<div class="wrap">
				<div class="logo">
					<svg id="Group_3283" data-name="Group 3283" xmlns="http://www.w3.org/2000/svg" width="138.045" height="164.466" viewBox="0 0 138.045 164.466">
						<g id="Group_56" data-name="Group 56" transform="translate(26.594 103.268)">
							<path id="Path_27" data-name="Path 27" d="M737.177,266.829H675a11.451,11.451,0,1,1,0-22.9h62.179a11.451,11.451,0,1,1,0,22.9Z" transform="translate(-663.544 -243.928)" fill="#0021c7"/>
						</g>
						<g id="Group_57" data-name="Group 57" transform="translate(78.321 34.609)">
							<path id="Path_28" data-name="Path 28" d="M721.058,254.57H684.233a11.451,11.451,0,1,1,0-22.9h36.825a11.451,11.451,0,0,1,0,22.9Z" transform="translate(-672.78 -231.669)" fill="#fe863b"/>
						</g>
						<g id="Group_58" data-name="Group 58" transform="translate(63.507 68.919)">
							<path id="Path_29" data-name="Path 29" d="M732.341,260.7H681.582a11.451,11.451,0,0,1,0-22.9h50.759a11.451,11.451,0,1,1,0,22.9Z" transform="translate(-670.135 -237.795)" fill="#03d07d"/>
						</g>
						<g id="Group_59" data-name="Group 59" transform="translate(0.92 68.919)">
							<path id="Path_30" data-name="Path 30" d="M698.629,260.7H670.408a11.451,11.451,0,0,1,0-22.9h28.221a11.451,11.451,0,0,1,0,22.9Z" transform="translate(-658.96 -237.795)" fill="#fce152"/>
						</g>
						<circle id="Ellipse_29" data-name="Ellipse 29" cx="11.458" cy="11.458" r="11.458" transform="translate(88.769 103.259)" fill="#000083"/>
						<circle id="Ellipse_30" data-name="Ellipse 30" cx="11.458" cy="11.458" r="11.458" transform="translate(63.496 68.913)" fill="#029c5e"/>
						<circle id="Ellipse_31" data-name="Ellipse 31" cx="11.458" cy="11.458" r="11.458" transform="translate(78.315 34.603)" fill="#e45a03"/>
						<g id="Group_60" data-name="Group 60" transform="translate(0.007 34.609)">
							<path id="Path_31" data-name="Path 31" d="M714.21,254.57H670.245a11.451,11.451,0,0,1,0-22.9H714.21a11.451,11.451,0,0,1,0,22.9Z" transform="translate(-658.797 -231.669)" fill="#f752d5"/>
						</g>
						<circle id="Ellipse_32" data-name="Ellipse 32" cx="11.458" cy="11.458" r="11.458" transform="translate(0 34.603)" fill="#de0c9d"/>
						<circle id="Ellipse_33" data-name="Ellipse 33" cx="11.458" cy="11.458" r="11.458" transform="translate(29.132 68.913)" fill="#fcb000"/>
						<g id="Group_61" data-name="Group 61" transform="translate(26.179 0.009)">
							<path id="Path_32" data-name="Path 32" d="M737.517,248.392h-62.6a11.451,11.451,0,0,1,0-22.9h62.6a11.451,11.451,0,1,1,0,22.9Z" transform="translate(-663.47 -225.491)" fill="#fc260c"/>
						</g>
						<circle id="Ellipse_34" data-name="Ellipse 34" cx="11.458" cy="11.458" r="11.458" transform="translate(88.769 0)" fill="#d52000"/>
						<path id="Path_66" data-name="Path 66" d="M686.3,275.569l-18.268-20.644a2.874,2.874,0,0,1,2.15-4.783h35.794a2.874,2.874,0,0,1,2.19,4.738l-17.519,20.644A2.878,2.878,0,0,1,686.3,275.569Z" transform="translate(-619.668 -112.072)" fill="#e6e6e6"/>
					</svg>
				</div>
				<div class="content">
					<h2>Now everyone can be a meeting planner.</h2>
					<p>TROOP is a micro service based technology which aggregates and visualizes the data required to organize a meeting, saving planning time and identifying options to save travel time, money and avoid CO2. Now anyone can organize a meeting in minutes rather than weeks using data instead of opinions.</p>
					<p><a href="/" class="button is-primary">READ MORE</a></p>
				</div>
			</div>
		</div>
	</div>
</div>